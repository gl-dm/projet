import { ICoop } from '@/shared/model/coop.model';
import { IProduct } from '@/shared/model/product.model';

export interface IRestaurant {
  id?: number;
  name?: string;
  address?: string;
  schedule?: string | null;
  phone?: string;
  deadline?: string | null;
  coop?: ICoop | null;
  products?: IProduct[] | null;
}

export class Restaurant implements IRestaurant {
  constructor(
    public id?: number,
    public name?: string,
    public address?: string,
    public schedule?: string | null,
    public phone?: string,
    public deadline?: string | null,
    public coop?: ICoop | null,
    public products?: IProduct[] | null
  ) {}
}
