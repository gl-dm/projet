import { Component, Vue, Inject } from 'vue-property-decorator';

import { IDeliveryRun } from '@/shared/model/delivery-run.model';
import DeliveryRunService from './delivery-run.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class DeliveryRunDetails extends Vue {
  @Inject('deliveryRunService') private deliveryRunService: () => DeliveryRunService;
  @Inject('alertService') private alertService: () => AlertService;

  public deliveryRun: IDeliveryRun = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.deliveryRunId) {
        vm.retrieveDeliveryRun(to.params.deliveryRunId);
      }
    });
  }

  public retrieveDeliveryRun(deliveryRunId) {
    this.deliveryRunService()
      .find(deliveryRunId)
      .then(res => {
        this.deliveryRun = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
