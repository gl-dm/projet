package polytech.info.gl.service.mapper;

import org.mapstruct.*;
import polytech.info.gl.domain.Courier;
import polytech.info.gl.domain.Customer;
import polytech.info.gl.domain.DeliveryRun;
import polytech.info.gl.domain.Order;
import polytech.info.gl.service.dto.CourierDTO;
import polytech.info.gl.service.dto.CustomerDTO;
import polytech.info.gl.service.dto.DeliveryRunDTO;
import polytech.info.gl.service.dto.OrderDTO;

/**
 * Mapper for the entity {@link Order} and its DTO {@link OrderDTO}.
 */
@Mapper(componentModel = "spring")
public interface OrderMapper extends EntityMapper<OrderDTO, Order> {
    @Mapping(target = "deliveryRun", source = "deliveryRun", qualifiedByName = "deliveryRunId")
    @Mapping(target = "customer", source = "customer", qualifiedByName = "customerId")
    @Mapping(target = "courier", source = "courier", qualifiedByName = "courierId")
    OrderDTO toDto(Order s);

    @Named("deliveryRunId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    DeliveryRunDTO toDtoDeliveryRunId(DeliveryRun deliveryRun);

    @Named("customerId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    CustomerDTO toDtoCustomerId(Customer customer);

    @Named("courierId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    CourierDTO toDtoCourierId(Courier courier);
}
