import { Component, Provide, Vue } from 'vue-property-decorator';

import UserService from '@/entities/user/user.service';
import RestaurantService from './restaurant/restaurant.service';
import CourierService from './courier/courier.service';
import CustomerService from './customer/customer.service';
import ProductService from './product/product.service';
import OrderService from './order/order.service';
import DeliveryRunService from './delivery-run/delivery-run.service';
import CoopService from './coop/coop.service';
// jhipster-needle-add-entity-service-to-entities-component-import - JHipster will import entities services here

@Component
export default class Entities extends Vue {
  @Provide('userService') private userService = () => new UserService();
  @Provide('restaurantService') private restaurantService = () => new RestaurantService();
  @Provide('courierService') private courierService = () => new CourierService();
  @Provide('customerService') private customerService = () => new CustomerService();
  @Provide('productService') private productService = () => new ProductService();
  @Provide('orderService') private orderService = () => new OrderService();
  @Provide('deliveryRunService') private deliveryRunService = () => new DeliveryRunService();
  @Provide('coopService') private coopService = () => new CoopService();
  // jhipster-needle-add-entity-service-to-entities-component - JHipster will import entities services here
}
