import { IOrder } from '@/shared/model/order.model';
import { IRestaurant } from '@/shared/model/restaurant.model';

export interface IProduct {
  id?: number;
  name?: string;
  description?: string | null;
  price?: number;
  category?: string;
  orders?: IOrder[] | null;
  restaurants?: IRestaurant[] | null;
}

export class Product implements IProduct {
  constructor(
    public id?: number,
    public name?: string,
    public description?: string | null,
    public price?: number,
    public category?: string,
    public orders?: IOrder[] | null,
    public restaurants?: IRestaurant[] | null
  ) {}
}
