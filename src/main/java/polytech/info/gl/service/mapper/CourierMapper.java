package polytech.info.gl.service.mapper;

import org.mapstruct.*;
import polytech.info.gl.domain.Coop;
import polytech.info.gl.domain.Courier;
import polytech.info.gl.service.dto.CoopDTO;
import polytech.info.gl.service.dto.CourierDTO;

/**
 * Mapper for the entity {@link Courier} and its DTO {@link CourierDTO}.
 */
@Mapper(componentModel = "spring")
public interface CourierMapper extends EntityMapper<CourierDTO, Courier> {
    @Mapping(target = "coop", source = "coop", qualifiedByName = "coopId")
    CourierDTO toDto(Courier s);

    @Named("coopId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    CoopDTO toDtoCoopId(Coop coop);
}
