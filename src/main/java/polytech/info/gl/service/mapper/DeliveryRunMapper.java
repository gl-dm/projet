package polytech.info.gl.service.mapper;

import org.mapstruct.*;
import polytech.info.gl.domain.Courier;
import polytech.info.gl.domain.DeliveryRun;
import polytech.info.gl.service.dto.CourierDTO;
import polytech.info.gl.service.dto.DeliveryRunDTO;

/**
 * Mapper for the entity {@link DeliveryRun} and its DTO {@link DeliveryRunDTO}.
 */
@Mapper(componentModel = "spring")
public interface DeliveryRunMapper extends EntityMapper<DeliveryRunDTO, DeliveryRun> {
    @Mapping(target = "coursier", source = "coursier", qualifiedByName = "courierId")
    DeliveryRunDTO toDto(DeliveryRun s);

    @Named("courierId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    CourierDTO toDtoCourierId(Courier courier);
}
