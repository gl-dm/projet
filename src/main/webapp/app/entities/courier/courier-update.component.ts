import { Component, Vue, Inject } from 'vue-property-decorator';

import { required, minLength, maxLength, numeric, minValue } from 'vuelidate/lib/validators';

import AlertService from '@/shared/alert/alert.service';

import CoopService from '@/entities/coop/coop.service';
import { ICoop } from '@/shared/model/coop.model';

import { ICourier, Courier } from '@/shared/model/courier.model';
import CourierService from './courier.service';

const validations: any = {
  courier: {
    nameC: {
      required,
      minLength: minLength(2),
      maxLength: maxLength(100),
    },
    idC: {
      numeric,
      min: minValue(1000),
    },
    phone: {
      required,
    },
    email: {
      required,
    },
    maxCapacity: {
      required,
      numeric,
      min: minValue(1),
    },
    deadline: {},
  },
};

@Component({
  validations,
})
export default class CourierUpdate extends Vue {
  @Inject('courierService') private courierService: () => CourierService;
  @Inject('alertService') private alertService: () => AlertService;

  public courier: ICourier = new Courier();

  @Inject('coopService') private coopService: () => CoopService;

  public coops: ICoop[] = [];
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.courierId) {
        vm.retrieveCourier(to.params.courierId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.courier.id) {
      this.courierService()
        .update(this.courier)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('blogApp.courier.updated', { param: param.id });
          return (this.$root as any).$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    } else {
      this.courierService()
        .create(this.courier)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('blogApp.courier.created', { param: param.id });
          (this.$root as any).$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    }
  }

  public retrieveCourier(courierId): void {
    this.courierService()
      .find(courierId)
      .then(res => {
        this.courier = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.coopService()
      .retrieve()
      .then(res => {
        this.coops = res.data;
      });
  }
}
