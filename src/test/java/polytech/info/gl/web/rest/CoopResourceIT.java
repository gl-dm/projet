package polytech.info.gl.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import polytech.info.gl.IntegrationTest;
import polytech.info.gl.domain.Coop;
import polytech.info.gl.repository.CoopRepository;
import polytech.info.gl.service.dto.CoopDTO;
import polytech.info.gl.service.mapper.CoopMapper;

/**
 * Integration tests for the {@link CoopResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class CoopResourceIT {

    private static final String DEFAULT_NAME_COOP = "AAAAAAAAAA";
    private static final String UPDATED_NAME_COOP = "BBBBBBBBBB";

    private static final Long DEFAULT_ID_COOP = 1L;
    private static final Long UPDATED_ID_COOP = 2L;

    private static final String DEFAULT_ZONE = "AAAAAAAAAA";
    private static final String UPDATED_ZONE = "BBBBBBBBBB";

    private static final String DEFAULT_ADDRESS_COOP = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS_COOP = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/coops";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private CoopRepository coopRepository;

    @Autowired
    private CoopMapper coopMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCoopMockMvc;

    private Coop coop;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Coop createEntity(EntityManager em) {
        Coop coop = new Coop().nameCoop(DEFAULT_NAME_COOP).idCoop(DEFAULT_ID_COOP).zone(DEFAULT_ZONE).addressCoop(DEFAULT_ADDRESS_COOP);
        return coop;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Coop createUpdatedEntity(EntityManager em) {
        Coop coop = new Coop().nameCoop(UPDATED_NAME_COOP).idCoop(UPDATED_ID_COOP).zone(UPDATED_ZONE).addressCoop(UPDATED_ADDRESS_COOP);
        return coop;
    }

    @BeforeEach
    public void initTest() {
        coop = createEntity(em);
    }

    @Test
    @Transactional
    void createCoop() throws Exception {
        int databaseSizeBeforeCreate = coopRepository.findAll().size();
        // Create the Coop
        CoopDTO coopDTO = coopMapper.toDto(coop);
        restCoopMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(coopDTO)))
            .andExpect(status().isCreated());

        // Validate the Coop in the database
        List<Coop> coopList = coopRepository.findAll();
        assertThat(coopList).hasSize(databaseSizeBeforeCreate + 1);
        Coop testCoop = coopList.get(coopList.size() - 1);
        assertThat(testCoop.getNameCoop()).isEqualTo(DEFAULT_NAME_COOP);
        assertThat(testCoop.getIdCoop()).isEqualTo(DEFAULT_ID_COOP);
        assertThat(testCoop.getZone()).isEqualTo(DEFAULT_ZONE);
        assertThat(testCoop.getAddressCoop()).isEqualTo(DEFAULT_ADDRESS_COOP);
    }

    @Test
    @Transactional
    void createCoopWithExistingId() throws Exception {
        // Create the Coop with an existing ID
        coop.setId(1L);
        CoopDTO coopDTO = coopMapper.toDto(coop);

        int databaseSizeBeforeCreate = coopRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restCoopMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(coopDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Coop in the database
        List<Coop> coopList = coopRepository.findAll();
        assertThat(coopList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkIdCoopIsRequired() throws Exception {
        int databaseSizeBeforeTest = coopRepository.findAll().size();
        // set the field null
        coop.setIdCoop(null);

        // Create the Coop, which fails.
        CoopDTO coopDTO = coopMapper.toDto(coop);

        restCoopMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(coopDTO)))
            .andExpect(status().isBadRequest());

        List<Coop> coopList = coopRepository.findAll();
        assertThat(coopList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllCoops() throws Exception {
        // Initialize the database
        coopRepository.saveAndFlush(coop);

        // Get all the coopList
        restCoopMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(coop.getId().intValue())))
            .andExpect(jsonPath("$.[*].nameCoop").value(hasItem(DEFAULT_NAME_COOP)))
            .andExpect(jsonPath("$.[*].idCoop").value(hasItem(DEFAULT_ID_COOP.intValue())))
            .andExpect(jsonPath("$.[*].zone").value(hasItem(DEFAULT_ZONE)))
            .andExpect(jsonPath("$.[*].addressCoop").value(hasItem(DEFAULT_ADDRESS_COOP)));
    }

    @Test
    @Transactional
    void getCoop() throws Exception {
        // Initialize the database
        coopRepository.saveAndFlush(coop);

        // Get the coop
        restCoopMockMvc
            .perform(get(ENTITY_API_URL_ID, coop.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(coop.getId().intValue()))
            .andExpect(jsonPath("$.nameCoop").value(DEFAULT_NAME_COOP))
            .andExpect(jsonPath("$.idCoop").value(DEFAULT_ID_COOP.intValue()))
            .andExpect(jsonPath("$.zone").value(DEFAULT_ZONE))
            .andExpect(jsonPath("$.addressCoop").value(DEFAULT_ADDRESS_COOP));
    }

    @Test
    @Transactional
    void getNonExistingCoop() throws Exception {
        // Get the coop
        restCoopMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingCoop() throws Exception {
        // Initialize the database
        coopRepository.saveAndFlush(coop);

        int databaseSizeBeforeUpdate = coopRepository.findAll().size();

        // Update the coop
        Coop updatedCoop = coopRepository.findById(coop.getId()).get();
        // Disconnect from session so that the updates on updatedCoop are not directly saved in db
        em.detach(updatedCoop);
        updatedCoop.nameCoop(UPDATED_NAME_COOP).idCoop(UPDATED_ID_COOP).zone(UPDATED_ZONE).addressCoop(UPDATED_ADDRESS_COOP);
        CoopDTO coopDTO = coopMapper.toDto(updatedCoop);

        restCoopMockMvc
            .perform(
                put(ENTITY_API_URL_ID, coopDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(coopDTO))
            )
            .andExpect(status().isOk());

        // Validate the Coop in the database
        List<Coop> coopList = coopRepository.findAll();
        assertThat(coopList).hasSize(databaseSizeBeforeUpdate);
        Coop testCoop = coopList.get(coopList.size() - 1);
        assertThat(testCoop.getNameCoop()).isEqualTo(UPDATED_NAME_COOP);
        assertThat(testCoop.getIdCoop()).isEqualTo(UPDATED_ID_COOP);
        assertThat(testCoop.getZone()).isEqualTo(UPDATED_ZONE);
        assertThat(testCoop.getAddressCoop()).isEqualTo(UPDATED_ADDRESS_COOP);
    }

    @Test
    @Transactional
    void putNonExistingCoop() throws Exception {
        int databaseSizeBeforeUpdate = coopRepository.findAll().size();
        coop.setId(count.incrementAndGet());

        // Create the Coop
        CoopDTO coopDTO = coopMapper.toDto(coop);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCoopMockMvc
            .perform(
                put(ENTITY_API_URL_ID, coopDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(coopDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Coop in the database
        List<Coop> coopList = coopRepository.findAll();
        assertThat(coopList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchCoop() throws Exception {
        int databaseSizeBeforeUpdate = coopRepository.findAll().size();
        coop.setId(count.incrementAndGet());

        // Create the Coop
        CoopDTO coopDTO = coopMapper.toDto(coop);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCoopMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(coopDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Coop in the database
        List<Coop> coopList = coopRepository.findAll();
        assertThat(coopList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamCoop() throws Exception {
        int databaseSizeBeforeUpdate = coopRepository.findAll().size();
        coop.setId(count.incrementAndGet());

        // Create the Coop
        CoopDTO coopDTO = coopMapper.toDto(coop);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCoopMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(coopDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Coop in the database
        List<Coop> coopList = coopRepository.findAll();
        assertThat(coopList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateCoopWithPatch() throws Exception {
        // Initialize the database
        coopRepository.saveAndFlush(coop);

        int databaseSizeBeforeUpdate = coopRepository.findAll().size();

        // Update the coop using partial update
        Coop partialUpdatedCoop = new Coop();
        partialUpdatedCoop.setId(coop.getId());

        partialUpdatedCoop.nameCoop(UPDATED_NAME_COOP).zone(UPDATED_ZONE);

        restCoopMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCoop.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCoop))
            )
            .andExpect(status().isOk());

        // Validate the Coop in the database
        List<Coop> coopList = coopRepository.findAll();
        assertThat(coopList).hasSize(databaseSizeBeforeUpdate);
        Coop testCoop = coopList.get(coopList.size() - 1);
        assertThat(testCoop.getNameCoop()).isEqualTo(UPDATED_NAME_COOP);
        assertThat(testCoop.getIdCoop()).isEqualTo(DEFAULT_ID_COOP);
        assertThat(testCoop.getZone()).isEqualTo(UPDATED_ZONE);
        assertThat(testCoop.getAddressCoop()).isEqualTo(DEFAULT_ADDRESS_COOP);
    }

    @Test
    @Transactional
    void fullUpdateCoopWithPatch() throws Exception {
        // Initialize the database
        coopRepository.saveAndFlush(coop);

        int databaseSizeBeforeUpdate = coopRepository.findAll().size();

        // Update the coop using partial update
        Coop partialUpdatedCoop = new Coop();
        partialUpdatedCoop.setId(coop.getId());

        partialUpdatedCoop.nameCoop(UPDATED_NAME_COOP).idCoop(UPDATED_ID_COOP).zone(UPDATED_ZONE).addressCoop(UPDATED_ADDRESS_COOP);

        restCoopMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCoop.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCoop))
            )
            .andExpect(status().isOk());

        // Validate the Coop in the database
        List<Coop> coopList = coopRepository.findAll();
        assertThat(coopList).hasSize(databaseSizeBeforeUpdate);
        Coop testCoop = coopList.get(coopList.size() - 1);
        assertThat(testCoop.getNameCoop()).isEqualTo(UPDATED_NAME_COOP);
        assertThat(testCoop.getIdCoop()).isEqualTo(UPDATED_ID_COOP);
        assertThat(testCoop.getZone()).isEqualTo(UPDATED_ZONE);
        assertThat(testCoop.getAddressCoop()).isEqualTo(UPDATED_ADDRESS_COOP);
    }

    @Test
    @Transactional
    void patchNonExistingCoop() throws Exception {
        int databaseSizeBeforeUpdate = coopRepository.findAll().size();
        coop.setId(count.incrementAndGet());

        // Create the Coop
        CoopDTO coopDTO = coopMapper.toDto(coop);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCoopMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, coopDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(coopDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Coop in the database
        List<Coop> coopList = coopRepository.findAll();
        assertThat(coopList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchCoop() throws Exception {
        int databaseSizeBeforeUpdate = coopRepository.findAll().size();
        coop.setId(count.incrementAndGet());

        // Create the Coop
        CoopDTO coopDTO = coopMapper.toDto(coop);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCoopMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(coopDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Coop in the database
        List<Coop> coopList = coopRepository.findAll();
        assertThat(coopList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamCoop() throws Exception {
        int databaseSizeBeforeUpdate = coopRepository.findAll().size();
        coop.setId(count.incrementAndGet());

        // Create the Coop
        CoopDTO coopDTO = coopMapper.toDto(coop);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCoopMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(coopDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Coop in the database
        List<Coop> coopList = coopRepository.findAll();
        assertThat(coopList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteCoop() throws Exception {
        // Initialize the database
        coopRepository.saveAndFlush(coop);

        int databaseSizeBeforeDelete = coopRepository.findAll().size();

        // Delete the coop
        restCoopMockMvc
            .perform(delete(ENTITY_API_URL_ID, coop.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Coop> coopList = coopRepository.findAll();
        assertThat(coopList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
