package polytech.info.gl.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import polytech.info.gl.web.rest.TestUtil;

class DeliveryRunDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(DeliveryRunDTO.class);
        DeliveryRunDTO deliveryRunDTO1 = new DeliveryRunDTO();
        deliveryRunDTO1.setId(1L);
        DeliveryRunDTO deliveryRunDTO2 = new DeliveryRunDTO();
        assertThat(deliveryRunDTO1).isNotEqualTo(deliveryRunDTO2);
        deliveryRunDTO2.setId(deliveryRunDTO1.getId());
        assertThat(deliveryRunDTO1).isEqualTo(deliveryRunDTO2);
        deliveryRunDTO2.setId(2L);
        assertThat(deliveryRunDTO1).isNotEqualTo(deliveryRunDTO2);
        deliveryRunDTO1.setId(null);
        assertThat(deliveryRunDTO1).isNotEqualTo(deliveryRunDTO2);
    }
}
