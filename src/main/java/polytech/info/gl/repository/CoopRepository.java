package polytech.info.gl.repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import polytech.info.gl.domain.Coop;

/**
 * Spring Data JPA repository for the Coop entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CoopRepository extends JpaRepository<Coop, Long> {}
