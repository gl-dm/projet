package polytech.info.gl.domain;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Courier.
 */
@Entity
@Table(name = "courier")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Courier implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Size(min = 2, max = 100)
    @Column(name = "name_c", length = 100, nullable = false)
    private String nameC;

    @Min(value = 1000L)
    @Column(name = "id_c", unique = true)
    private Long idC;

    @NotNull
    @Pattern(regexp = "^(\\+)?(\\d|\\s|\\(|\\)|-){10,}$")
    @Column(name = "phone", nullable = false)
    private String phone;

    @NotNull
    @Pattern(regexp = "^[^\\s@]+@[^\\s@]+\\.[^\\s@]+$")
    @Column(name = "email", nullable = false)
    private String email;

    @NotNull
    @Min(value = 1)
    @Column(name = "max_capacity", nullable = false)
    private Integer maxCapacity;

    @Column(name = "deadline")
    private String deadline;

    @ManyToOne
    private Coop coop;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Courier id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNameC() {
        return this.nameC;
    }

    public Courier nameC(String nameC) {
        this.setNameC(nameC);
        return this;
    }

    public void setNameC(String nameC) {
        this.nameC = nameC;
    }

    public Long getIdC() {
        return this.idC;
    }

    public Courier idC(Long idC) {
        this.setIdC(idC);
        return this;
    }

    public void setIdC(Long idC) {
        this.idC = idC;
    }

    public String getPhone() {
        return this.phone;
    }

    public Courier phone(String phone) {
        this.setPhone(phone);
        return this;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return this.email;
    }

    public Courier email(String email) {
        this.setEmail(email);
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getMaxCapacity() {
        return this.maxCapacity;
    }

    public Courier maxCapacity(Integer maxCapacity) {
        this.setMaxCapacity(maxCapacity);
        return this;
    }

    public void setMaxCapacity(Integer maxCapacity) {
        this.maxCapacity = maxCapacity;
    }

    public String getDeadline() {
        return this.deadline;
    }

    public Courier deadline(String deadline) {
        this.setDeadline(deadline);
        return this;
    }

    public void setDeadline(String deadline) {
        this.deadline = deadline;
    }

    public Coop getCoop() {
        return this.coop;
    }

    public void setCoop(Coop coop) {
        this.coop = coop;
    }

    public Courier coop(Coop coop) {
        this.setCoop(coop);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Courier)) {
            return false;
        }
        return id != null && id.equals(((Courier) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Courier{" +
            "id=" + getId() +
            ", nameC='" + getNameC() + "'" +
            ", idC=" + getIdC() +
            ", phone='" + getPhone() + "'" +
            ", email='" + getEmail() + "'" +
            ", maxCapacity=" + getMaxCapacity() +
            ", deadline='" + getDeadline() + "'" +
            "}";
    }
}
