import { Component, Vue, Inject } from 'vue-property-decorator';

import { numeric, minLength } from 'vuelidate/lib/validators';
import dayjs from 'dayjs';
import { DATE_TIME_LONG_FORMAT } from '@/shared/date/filters';

import AlertService from '@/shared/alert/alert.service';

import CourierService from '@/entities/courier/courier.service';
import { ICourier } from '@/shared/model/courier.model';

import OrderService from '@/entities/order/order.service';
import { IOrder } from '@/shared/model/order.model';

import { IDeliveryRun, DeliveryRun } from '@/shared/model/delivery-run.model';
import DeliveryRunService from './delivery-run.service';

const validations: any = {
  deliveryRun: {
    idDelivery: {
      numeric,
    },
    address: {
      minLength: minLength(3),
    },
    startTime: {},
    endTime: {},
  },
};

@Component({
  validations,
})
export default class DeliveryRunUpdate extends Vue {
  @Inject('deliveryRunService') private deliveryRunService: () => DeliveryRunService;
  @Inject('alertService') private alertService: () => AlertService;

  public deliveryRun: IDeliveryRun = new DeliveryRun();

  @Inject('courierService') private courierService: () => CourierService;

  public couriers: ICourier[] = [];

  @Inject('orderService') private orderService: () => OrderService;

  public orders: IOrder[] = [];
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.deliveryRunId) {
        vm.retrieveDeliveryRun(to.params.deliveryRunId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.deliveryRun.id) {
      this.deliveryRunService()
        .update(this.deliveryRun)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('blogApp.deliveryRun.updated', { param: param.id });
          return (this.$root as any).$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    } else {
      this.deliveryRunService()
        .create(this.deliveryRun)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('blogApp.deliveryRun.created', { param: param.id });
          (this.$root as any).$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    }
  }

  public convertDateTimeFromServer(date: Date): string {
    if (date && dayjs(date).isValid()) {
      return dayjs(date).format(DATE_TIME_LONG_FORMAT);
    }
    return null;
  }

  public updateInstantField(field, event) {
    if (event.target.value) {
      this.deliveryRun[field] = dayjs(event.target.value, DATE_TIME_LONG_FORMAT);
    } else {
      this.deliveryRun[field] = null;
    }
  }

  public updateZonedDateTimeField(field, event) {
    if (event.target.value) {
      this.deliveryRun[field] = dayjs(event.target.value, DATE_TIME_LONG_FORMAT);
    } else {
      this.deliveryRun[field] = null;
    }
  }

  public retrieveDeliveryRun(deliveryRunId): void {
    this.deliveryRunService()
      .find(deliveryRunId)
      .then(res => {
        res.startTime = new Date(res.startTime);
        res.endTime = new Date(res.endTime);
        this.deliveryRun = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.courierService()
      .retrieve()
      .then(res => {
        this.couriers = res.data;
      });
    this.orderService()
      .retrieve()
      .then(res => {
        this.orders = res.data;
      });
  }
}
