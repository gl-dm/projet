package polytech.info.gl.service.dto;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link polytech.info.gl.domain.DeliveryRun} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class DeliveryRunDTO implements Serializable {

    private Long id;

    private Long idDelivery;

    @Size(min = 3)
    private String address;

    private Instant startTime;

    private Instant endTime;

    private CourierDTO coursier;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdDelivery() {
        return idDelivery;
    }

    public void setIdDelivery(Long idDelivery) {
        this.idDelivery = idDelivery;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Instant getStartTime() {
        return startTime;
    }

    public void setStartTime(Instant startTime) {
        this.startTime = startTime;
    }

    public Instant getEndTime() {
        return endTime;
    }

    public void setEndTime(Instant endTime) {
        this.endTime = endTime;
    }

    public CourierDTO getCoursier() {
        return coursier;
    }

    public void setCoursier(CourierDTO coursier) {
        this.coursier = coursier;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DeliveryRunDTO)) {
            return false;
        }

        DeliveryRunDTO deliveryRunDTO = (DeliveryRunDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, deliveryRunDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "DeliveryRunDTO{" +
            "id=" + getId() +
            ", idDelivery=" + getIdDelivery() +
            ", address='" + getAddress() + "'" +
            ", startTime='" + getStartTime() + "'" +
            ", endTime='" + getEndTime() + "'" +
            ", coursier=" + getCoursier() +
            "}";
    }
}
