package polytech.info.gl.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import polytech.info.gl.IntegrationTest;
import polytech.info.gl.domain.DeliveryRun;
import polytech.info.gl.repository.DeliveryRunRepository;
import polytech.info.gl.service.dto.DeliveryRunDTO;
import polytech.info.gl.service.mapper.DeliveryRunMapper;

/**
 * Integration tests for the {@link DeliveryRunResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class DeliveryRunResourceIT {

    private static final Long DEFAULT_ID_DELIVERY = 1L;
    private static final Long UPDATED_ID_DELIVERY = 2L;

    private static final String DEFAULT_ADDRESS = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS = "BBBBBBBBBB";

    private static final Instant DEFAULT_START_TIME = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_START_TIME = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_END_TIME = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_END_TIME = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String ENTITY_API_URL = "/api/delivery-runs";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private DeliveryRunRepository deliveryRunRepository;

    @Autowired
    private DeliveryRunMapper deliveryRunMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restDeliveryRunMockMvc;

    private DeliveryRun deliveryRun;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DeliveryRun createEntity(EntityManager em) {
        DeliveryRun deliveryRun = new DeliveryRun()
            .idDelivery(DEFAULT_ID_DELIVERY)
            .address(DEFAULT_ADDRESS)
            .startTime(DEFAULT_START_TIME)
            .endTime(DEFAULT_END_TIME);
        return deliveryRun;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DeliveryRun createUpdatedEntity(EntityManager em) {
        DeliveryRun deliveryRun = new DeliveryRun()
            .idDelivery(UPDATED_ID_DELIVERY)
            .address(UPDATED_ADDRESS)
            .startTime(UPDATED_START_TIME)
            .endTime(UPDATED_END_TIME);
        return deliveryRun;
    }

    @BeforeEach
    public void initTest() {
        deliveryRun = createEntity(em);
    }

    @Test
    @Transactional
    void createDeliveryRun() throws Exception {
        int databaseSizeBeforeCreate = deliveryRunRepository.findAll().size();
        // Create the DeliveryRun
        DeliveryRunDTO deliveryRunDTO = deliveryRunMapper.toDto(deliveryRun);
        restDeliveryRunMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(deliveryRunDTO))
            )
            .andExpect(status().isCreated());

        // Validate the DeliveryRun in the database
        List<DeliveryRun> deliveryRunList = deliveryRunRepository.findAll();
        assertThat(deliveryRunList).hasSize(databaseSizeBeforeCreate + 1);
        DeliveryRun testDeliveryRun = deliveryRunList.get(deliveryRunList.size() - 1);
        assertThat(testDeliveryRun.getIdDelivery()).isEqualTo(DEFAULT_ID_DELIVERY);
        assertThat(testDeliveryRun.getAddress()).isEqualTo(DEFAULT_ADDRESS);
        assertThat(testDeliveryRun.getStartTime()).isEqualTo(DEFAULT_START_TIME);
        assertThat(testDeliveryRun.getEndTime()).isEqualTo(DEFAULT_END_TIME);
    }

    @Test
    @Transactional
    void createDeliveryRunWithExistingId() throws Exception {
        // Create the DeliveryRun with an existing ID
        deliveryRun.setId(1L);
        DeliveryRunDTO deliveryRunDTO = deliveryRunMapper.toDto(deliveryRun);

        int databaseSizeBeforeCreate = deliveryRunRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restDeliveryRunMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(deliveryRunDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the DeliveryRun in the database
        List<DeliveryRun> deliveryRunList = deliveryRunRepository.findAll();
        assertThat(deliveryRunList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllDeliveryRuns() throws Exception {
        // Initialize the database
        deliveryRunRepository.saveAndFlush(deliveryRun);

        // Get all the deliveryRunList
        restDeliveryRunMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(deliveryRun.getId().intValue())))
            .andExpect(jsonPath("$.[*].idDelivery").value(hasItem(DEFAULT_ID_DELIVERY.intValue())))
            .andExpect(jsonPath("$.[*].address").value(hasItem(DEFAULT_ADDRESS)))
            .andExpect(jsonPath("$.[*].startTime").value(hasItem(DEFAULT_START_TIME.toString())))
            .andExpect(jsonPath("$.[*].endTime").value(hasItem(DEFAULT_END_TIME.toString())));
    }

    @Test
    @Transactional
    void getDeliveryRun() throws Exception {
        // Initialize the database
        deliveryRunRepository.saveAndFlush(deliveryRun);

        // Get the deliveryRun
        restDeliveryRunMockMvc
            .perform(get(ENTITY_API_URL_ID, deliveryRun.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(deliveryRun.getId().intValue()))
            .andExpect(jsonPath("$.idDelivery").value(DEFAULT_ID_DELIVERY.intValue()))
            .andExpect(jsonPath("$.address").value(DEFAULT_ADDRESS))
            .andExpect(jsonPath("$.startTime").value(DEFAULT_START_TIME.toString()))
            .andExpect(jsonPath("$.endTime").value(DEFAULT_END_TIME.toString()));
    }

    @Test
    @Transactional
    void getNonExistingDeliveryRun() throws Exception {
        // Get the deliveryRun
        restDeliveryRunMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingDeliveryRun() throws Exception {
        // Initialize the database
        deliveryRunRepository.saveAndFlush(deliveryRun);

        int databaseSizeBeforeUpdate = deliveryRunRepository.findAll().size();

        // Update the deliveryRun
        DeliveryRun updatedDeliveryRun = deliveryRunRepository.findById(deliveryRun.getId()).get();
        // Disconnect from session so that the updates on updatedDeliveryRun are not directly saved in db
        em.detach(updatedDeliveryRun);
        updatedDeliveryRun.idDelivery(UPDATED_ID_DELIVERY).address(UPDATED_ADDRESS).startTime(UPDATED_START_TIME).endTime(UPDATED_END_TIME);
        DeliveryRunDTO deliveryRunDTO = deliveryRunMapper.toDto(updatedDeliveryRun);

        restDeliveryRunMockMvc
            .perform(
                put(ENTITY_API_URL_ID, deliveryRunDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(deliveryRunDTO))
            )
            .andExpect(status().isOk());

        // Validate the DeliveryRun in the database
        List<DeliveryRun> deliveryRunList = deliveryRunRepository.findAll();
        assertThat(deliveryRunList).hasSize(databaseSizeBeforeUpdate);
        DeliveryRun testDeliveryRun = deliveryRunList.get(deliveryRunList.size() - 1);
        assertThat(testDeliveryRun.getIdDelivery()).isEqualTo(UPDATED_ID_DELIVERY);
        assertThat(testDeliveryRun.getAddress()).isEqualTo(UPDATED_ADDRESS);
        assertThat(testDeliveryRun.getStartTime()).isEqualTo(UPDATED_START_TIME);
        assertThat(testDeliveryRun.getEndTime()).isEqualTo(UPDATED_END_TIME);
    }

    @Test
    @Transactional
    void putNonExistingDeliveryRun() throws Exception {
        int databaseSizeBeforeUpdate = deliveryRunRepository.findAll().size();
        deliveryRun.setId(count.incrementAndGet());

        // Create the DeliveryRun
        DeliveryRunDTO deliveryRunDTO = deliveryRunMapper.toDto(deliveryRun);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDeliveryRunMockMvc
            .perform(
                put(ENTITY_API_URL_ID, deliveryRunDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(deliveryRunDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the DeliveryRun in the database
        List<DeliveryRun> deliveryRunList = deliveryRunRepository.findAll();
        assertThat(deliveryRunList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchDeliveryRun() throws Exception {
        int databaseSizeBeforeUpdate = deliveryRunRepository.findAll().size();
        deliveryRun.setId(count.incrementAndGet());

        // Create the DeliveryRun
        DeliveryRunDTO deliveryRunDTO = deliveryRunMapper.toDto(deliveryRun);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDeliveryRunMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(deliveryRunDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the DeliveryRun in the database
        List<DeliveryRun> deliveryRunList = deliveryRunRepository.findAll();
        assertThat(deliveryRunList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamDeliveryRun() throws Exception {
        int databaseSizeBeforeUpdate = deliveryRunRepository.findAll().size();
        deliveryRun.setId(count.incrementAndGet());

        // Create the DeliveryRun
        DeliveryRunDTO deliveryRunDTO = deliveryRunMapper.toDto(deliveryRun);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDeliveryRunMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(deliveryRunDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the DeliveryRun in the database
        List<DeliveryRun> deliveryRunList = deliveryRunRepository.findAll();
        assertThat(deliveryRunList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateDeliveryRunWithPatch() throws Exception {
        // Initialize the database
        deliveryRunRepository.saveAndFlush(deliveryRun);

        int databaseSizeBeforeUpdate = deliveryRunRepository.findAll().size();

        // Update the deliveryRun using partial update
        DeliveryRun partialUpdatedDeliveryRun = new DeliveryRun();
        partialUpdatedDeliveryRun.setId(deliveryRun.getId());

        partialUpdatedDeliveryRun.idDelivery(UPDATED_ID_DELIVERY).address(UPDATED_ADDRESS).startTime(UPDATED_START_TIME);

        restDeliveryRunMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedDeliveryRun.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedDeliveryRun))
            )
            .andExpect(status().isOk());

        // Validate the DeliveryRun in the database
        List<DeliveryRun> deliveryRunList = deliveryRunRepository.findAll();
        assertThat(deliveryRunList).hasSize(databaseSizeBeforeUpdate);
        DeliveryRun testDeliveryRun = deliveryRunList.get(deliveryRunList.size() - 1);
        assertThat(testDeliveryRun.getIdDelivery()).isEqualTo(UPDATED_ID_DELIVERY);
        assertThat(testDeliveryRun.getAddress()).isEqualTo(UPDATED_ADDRESS);
        assertThat(testDeliveryRun.getStartTime()).isEqualTo(UPDATED_START_TIME);
        assertThat(testDeliveryRun.getEndTime()).isEqualTo(DEFAULT_END_TIME);
    }

    @Test
    @Transactional
    void fullUpdateDeliveryRunWithPatch() throws Exception {
        // Initialize the database
        deliveryRunRepository.saveAndFlush(deliveryRun);

        int databaseSizeBeforeUpdate = deliveryRunRepository.findAll().size();

        // Update the deliveryRun using partial update
        DeliveryRun partialUpdatedDeliveryRun = new DeliveryRun();
        partialUpdatedDeliveryRun.setId(deliveryRun.getId());

        partialUpdatedDeliveryRun
            .idDelivery(UPDATED_ID_DELIVERY)
            .address(UPDATED_ADDRESS)
            .startTime(UPDATED_START_TIME)
            .endTime(UPDATED_END_TIME);

        restDeliveryRunMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedDeliveryRun.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedDeliveryRun))
            )
            .andExpect(status().isOk());

        // Validate the DeliveryRun in the database
        List<DeliveryRun> deliveryRunList = deliveryRunRepository.findAll();
        assertThat(deliveryRunList).hasSize(databaseSizeBeforeUpdate);
        DeliveryRun testDeliveryRun = deliveryRunList.get(deliveryRunList.size() - 1);
        assertThat(testDeliveryRun.getIdDelivery()).isEqualTo(UPDATED_ID_DELIVERY);
        assertThat(testDeliveryRun.getAddress()).isEqualTo(UPDATED_ADDRESS);
        assertThat(testDeliveryRun.getStartTime()).isEqualTo(UPDATED_START_TIME);
        assertThat(testDeliveryRun.getEndTime()).isEqualTo(UPDATED_END_TIME);
    }

    @Test
    @Transactional
    void patchNonExistingDeliveryRun() throws Exception {
        int databaseSizeBeforeUpdate = deliveryRunRepository.findAll().size();
        deliveryRun.setId(count.incrementAndGet());

        // Create the DeliveryRun
        DeliveryRunDTO deliveryRunDTO = deliveryRunMapper.toDto(deliveryRun);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDeliveryRunMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, deliveryRunDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(deliveryRunDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the DeliveryRun in the database
        List<DeliveryRun> deliveryRunList = deliveryRunRepository.findAll();
        assertThat(deliveryRunList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchDeliveryRun() throws Exception {
        int databaseSizeBeforeUpdate = deliveryRunRepository.findAll().size();
        deliveryRun.setId(count.incrementAndGet());

        // Create the DeliveryRun
        DeliveryRunDTO deliveryRunDTO = deliveryRunMapper.toDto(deliveryRun);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDeliveryRunMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(deliveryRunDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the DeliveryRun in the database
        List<DeliveryRun> deliveryRunList = deliveryRunRepository.findAll();
        assertThat(deliveryRunList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamDeliveryRun() throws Exception {
        int databaseSizeBeforeUpdate = deliveryRunRepository.findAll().size();
        deliveryRun.setId(count.incrementAndGet());

        // Create the DeliveryRun
        DeliveryRunDTO deliveryRunDTO = deliveryRunMapper.toDto(deliveryRun);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDeliveryRunMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(deliveryRunDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the DeliveryRun in the database
        List<DeliveryRun> deliveryRunList = deliveryRunRepository.findAll();
        assertThat(deliveryRunList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteDeliveryRun() throws Exception {
        // Initialize the database
        deliveryRunRepository.saveAndFlush(deliveryRun);

        int databaseSizeBeforeDelete = deliveryRunRepository.findAll().size();

        // Delete the deliveryRun
        restDeliveryRunMockMvc
            .perform(delete(ENTITY_API_URL_ID, deliveryRun.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<DeliveryRun> deliveryRunList = deliveryRunRepository.findAll();
        assertThat(deliveryRunList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
