package polytech.info.gl.domain;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Coop.
 */
@Entity
@Table(name = "coop")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Coop implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "name_coop")
    private String nameCoop;

    @NotNull
    @Column(name = "id_coop", nullable = false, unique = true)
    private Long idCoop;

    @Column(name = "zone")
    private String zone;

    @Column(name = "address_coop")
    private String addressCoop;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Coop id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNameCoop() {
        return this.nameCoop;
    }

    public Coop nameCoop(String nameCoop) {
        this.setNameCoop(nameCoop);
        return this;
    }

    public void setNameCoop(String nameCoop) {
        this.nameCoop = nameCoop;
    }

    public Long getIdCoop() {
        return this.idCoop;
    }

    public Coop idCoop(Long idCoop) {
        this.setIdCoop(idCoop);
        return this;
    }

    public void setIdCoop(Long idCoop) {
        this.idCoop = idCoop;
    }

    public String getZone() {
        return this.zone;
    }

    public Coop zone(String zone) {
        this.setZone(zone);
        return this;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }

    public String getAddressCoop() {
        return this.addressCoop;
    }

    public Coop addressCoop(String addressCoop) {
        this.setAddressCoop(addressCoop);
        return this;
    }

    public void setAddressCoop(String addressCoop) {
        this.addressCoop = addressCoop;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Coop)) {
            return false;
        }
        return id != null && id.equals(((Coop) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Coop{" +
            "id=" + getId() +
            ", nameCoop='" + getNameCoop() + "'" +
            ", idCoop=" + getIdCoop() +
            ", zone='" + getZone() + "'" +
            ", addressCoop='" + getAddressCoop() + "'" +
            "}";
    }
}
