import { ICourier } from '@/shared/model/courier.model';
import { IOrder } from '@/shared/model/order.model';

export interface IDeliveryRun {
  id?: number;
  idDelivery?: number | null;
  address?: string | null;
  startTime?: Date | null;
  endTime?: Date | null;
  coursier?: ICourier | null;
  order?: IOrder | null;
}

export class DeliveryRun implements IDeliveryRun {
  constructor(
    public id?: number,
    public idDelivery?: number | null,
    public address?: string | null,
    public startTime?: Date | null,
    public endTime?: Date | null,
    public coursier?: ICourier | null,
    public order?: IOrder | null
  ) {}
}
