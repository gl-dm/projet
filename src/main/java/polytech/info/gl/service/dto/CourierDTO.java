package polytech.info.gl.service.dto;

import java.io.Serializable;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link polytech.info.gl.domain.Courier} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class CourierDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(min = 2, max = 100)
    private String nameC;

    @Min(value = 1000L)
    private Long idC;

    @NotNull
    @Pattern(regexp = "^(\\+)?(\\d|\\s|\\(|\\)|-){10,}$")
    private String phone;

    @NotNull
    @Pattern(regexp = "^[^\\s@]+@[^\\s@]+\\.[^\\s@]+$")
    private String email;

    @NotNull
    @Min(value = 1)
    private Integer maxCapacity;

    private String deadline;

    private CoopDTO coop;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNameC() {
        return nameC;
    }

    public void setNameC(String nameC) {
        this.nameC = nameC;
    }

    public Long getIdC() {
        return idC;
    }

    public void setIdC(Long idC) {
        this.idC = idC;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getMaxCapacity() {
        return maxCapacity;
    }

    public void setMaxCapacity(Integer maxCapacity) {
        this.maxCapacity = maxCapacity;
    }

    public String getDeadline() {
        return deadline;
    }

    public void setDeadline(String deadline) {
        this.deadline = deadline;
    }

    public CoopDTO getCoop() {
        return coop;
    }

    public void setCoop(CoopDTO coop) {
        this.coop = coop;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CourierDTO)) {
            return false;
        }

        CourierDTO courierDTO = (CourierDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, courierDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CourierDTO{" +
            "id=" + getId() +
            ", nameC='" + getNameC() + "'" +
            ", idC=" + getIdC() +
            ", phone='" + getPhone() + "'" +
            ", email='" + getEmail() + "'" +
            ", maxCapacity=" + getMaxCapacity() +
            ", deadline='" + getDeadline() + "'" +
            ", coop=" + getCoop() +
            "}";
    }
}
