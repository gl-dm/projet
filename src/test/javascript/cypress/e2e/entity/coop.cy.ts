import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('Coop e2e test', () => {
  const coopPageUrl = '/coop';
  const coopPageUrlPattern = new RegExp('/coop(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'user';
  const password = Cypress.env('E2E_PASSWORD') ?? 'user';
  const coopSample = { idCoop: 82377 };

  let coop;

  beforeEach(() => {
    cy.login(username, password);
  });

  beforeEach(() => {
    cy.intercept('GET', '/api/coops+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/api/coops').as('postEntityRequest');
    cy.intercept('DELETE', '/api/coops/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (coop) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/api/coops/${coop.id}`,
      }).then(() => {
        coop = undefined;
      });
    }
  });

  it('Coops menu should load Coops page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('coop');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('Coop').should('exist');
    cy.url().should('match', coopPageUrlPattern);
  });

  describe('Coop page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(coopPageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create Coop page', () => {
        cy.get(entityCreateButtonSelector).click();
        cy.url().should('match', new RegExp('/coop/new$'));
        cy.getEntityCreateUpdateHeading('Coop');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', coopPageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/api/coops',
          body: coopSample,
        }).then(({ body }) => {
          coop = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/api/coops+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              body: [coop],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(coopPageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details Coop page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('coop');
        cy.get(entityDetailsBackButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', coopPageUrlPattern);
      });

      it('edit button click should load edit Coop page and go back', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('Coop');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', coopPageUrlPattern);
      });

      it('edit button click should load edit Coop page and save', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('Coop');
        cy.get(entityCreateSaveButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', coopPageUrlPattern);
      });

      it('last delete button click should delete instance of Coop', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('coop').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click();
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', coopPageUrlPattern);

        coop = undefined;
      });
    });
  });

  describe('new Coop page', () => {
    beforeEach(() => {
      cy.visit(`${coopPageUrl}`);
      cy.get(entityCreateButtonSelector).click();
      cy.getEntityCreateUpdateHeading('Coop');
    });

    it('should create an instance of Coop', () => {
      cy.get(`[data-cy="nameCoop"]`).type('deposit Loan').should('have.value', 'deposit Loan');

      cy.get(`[data-cy="idCoop"]`).type('40680').should('have.value', '40680');

      cy.get(`[data-cy="zone"]`).type('schemas').should('have.value', 'schemas');

      cy.get(`[data-cy="addressCoop"]`).type('a').should('have.value', 'a');

      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response.statusCode).to.equal(201);
        coop = response.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response.statusCode).to.equal(200);
      });
      cy.url().should('match', coopPageUrlPattern);
    });
  });
});
