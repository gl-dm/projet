package polytech.info.gl.domain;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import polytech.info.gl.web.rest.TestUtil;

class CoopTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Coop.class);
        Coop coop1 = new Coop();
        coop1.setId(1L);
        Coop coop2 = new Coop();
        coop2.setId(coop1.getId());
        assertThat(coop1).isEqualTo(coop2);
        coop2.setId(2L);
        assertThat(coop1).isNotEqualTo(coop2);
        coop1.setId(null);
        assertThat(coop1).isNotEqualTo(coop2);
    }
}
