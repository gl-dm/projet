package polytech.info.gl.repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import polytech.info.gl.domain.DeliveryRun;

/**
 * Spring Data JPA repository for the DeliveryRun entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DeliveryRunRepository extends JpaRepository<DeliveryRun, Long> {}
