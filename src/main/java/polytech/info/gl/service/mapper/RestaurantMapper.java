package polytech.info.gl.service.mapper;

import java.util.Set;
import java.util.stream.Collectors;
import org.mapstruct.*;
import polytech.info.gl.domain.Coop;
import polytech.info.gl.domain.Product;
import polytech.info.gl.domain.Restaurant;
import polytech.info.gl.service.dto.CoopDTO;
import polytech.info.gl.service.dto.ProductDTO;
import polytech.info.gl.service.dto.RestaurantDTO;

/**
 * Mapper for the entity {@link Restaurant} and its DTO {@link RestaurantDTO}.
 */
@Mapper(componentModel = "spring")
public interface RestaurantMapper extends EntityMapper<RestaurantDTO, Restaurant> {
    @Mapping(target = "coop", source = "coop", qualifiedByName = "coopId")
    @Mapping(target = "products", source = "products", qualifiedByName = "productIdSet")
    RestaurantDTO toDto(Restaurant s);

    @Mapping(target = "removeProduct", ignore = true)
    Restaurant toEntity(RestaurantDTO restaurantDTO);

    @Named("coopId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    CoopDTO toDtoCoopId(Coop coop);

    @Named("productId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    ProductDTO toDtoProductId(Product product);

    @Named("productIdSet")
    default Set<ProductDTO> toDtoProductIdSet(Set<Product> product) {
        return product.stream().map(this::toDtoProductId).collect(Collectors.toSet());
    }
}
