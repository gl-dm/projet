export interface ICoop {
  id?: number;
  nameCoop?: string | null;
  idCoop?: number;
  zone?: string | null;
  addressCoop?: string | null;
}

export class Coop implements ICoop {
  constructor(
    public id?: number,
    public nameCoop?: string | null,
    public idCoop?: number,
    public zone?: string | null,
    public addressCoop?: string | null
  ) {}
}
