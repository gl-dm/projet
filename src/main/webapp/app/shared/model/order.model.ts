import { IDeliveryRun } from '@/shared/model/delivery-run.model';
import { ICustomer } from '@/shared/model/customer.model';
import { ICourier } from '@/shared/model/courier.model';
import { IProduct } from '@/shared/model/product.model';

export interface IOrder {
  id?: number;
  idOrder?: number | null;
  price?: number;
  status?: string | null;
  deliveryRun?: IDeliveryRun | null;
  customer?: ICustomer | null;
  courier?: ICourier | null;
  products?: IProduct[] | null;
}

export class Order implements IOrder {
  constructor(
    public id?: number,
    public idOrder?: number | null,
    public price?: number,
    public status?: string | null,
    public deliveryRun?: IDeliveryRun | null,
    public customer?: ICustomer | null,
    public courier?: ICourier | null,
    public products?: IProduct[] | null
  ) {}
}
