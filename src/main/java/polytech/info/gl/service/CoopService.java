package polytech.info.gl.service;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import polytech.info.gl.domain.Coop;
import polytech.info.gl.repository.CoopRepository;
import polytech.info.gl.service.dto.CoopDTO;
import polytech.info.gl.service.mapper.CoopMapper;

/**
 * Service Implementation for managing {@link Coop}.
 */
@Service
@Transactional
public class CoopService {

    private final Logger log = LoggerFactory.getLogger(CoopService.class);

    private final CoopRepository coopRepository;

    private final CoopMapper coopMapper;

    public CoopService(CoopRepository coopRepository, CoopMapper coopMapper) {
        this.coopRepository = coopRepository;
        this.coopMapper = coopMapper;
    }

    /**
     * Save a coop.
     *
     * @param coopDTO the entity to save.
     * @return the persisted entity.
     */
    public CoopDTO save(CoopDTO coopDTO) {
        log.debug("Request to save Coop : {}", coopDTO);
        Coop coop = coopMapper.toEntity(coopDTO);
        coop = coopRepository.save(coop);
        return coopMapper.toDto(coop);
    }

    /**
     * Update a coop.
     *
     * @param coopDTO the entity to save.
     * @return the persisted entity.
     */
    public CoopDTO update(CoopDTO coopDTO) {
        log.debug("Request to update Coop : {}", coopDTO);
        Coop coop = coopMapper.toEntity(coopDTO);
        coop = coopRepository.save(coop);
        return coopMapper.toDto(coop);
    }

    /**
     * Partially update a coop.
     *
     * @param coopDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<CoopDTO> partialUpdate(CoopDTO coopDTO) {
        log.debug("Request to partially update Coop : {}", coopDTO);

        return coopRepository
            .findById(coopDTO.getId())
            .map(existingCoop -> {
                coopMapper.partialUpdate(existingCoop, coopDTO);

                return existingCoop;
            })
            .map(coopRepository::save)
            .map(coopMapper::toDto);
    }

    /**
     * Get all the coops.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<CoopDTO> findAll() {
        log.debug("Request to get all Coops");
        return coopRepository.findAll().stream().map(coopMapper::toDto).collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one coop by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<CoopDTO> findOne(Long id) {
        log.debug("Request to get Coop : {}", id);
        return coopRepository.findById(id).map(coopMapper::toDto);
    }

    /**
     * Delete the coop by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Coop : {}", id);
        coopRepository.deleteById(id);
    }
}
