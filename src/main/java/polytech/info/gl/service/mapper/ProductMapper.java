package polytech.info.gl.service.mapper;

import java.util.Set;
import java.util.stream.Collectors;
import org.mapstruct.*;
import polytech.info.gl.domain.Order;
import polytech.info.gl.domain.Product;
import polytech.info.gl.service.dto.OrderDTO;
import polytech.info.gl.service.dto.ProductDTO;

/**
 * Mapper for the entity {@link Product} and its DTO {@link ProductDTO}.
 */
@Mapper(componentModel = "spring")
public interface ProductMapper extends EntityMapper<ProductDTO, Product> {
    @Mapping(target = "orders", source = "orders", qualifiedByName = "orderIdSet")
    ProductDTO toDto(Product s);

    @Mapping(target = "removeOrder", ignore = true)
    Product toEntity(ProductDTO productDTO);

    @Named("orderId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    OrderDTO toDtoOrderId(Order order);

    @Named("orderIdSet")
    default Set<OrderDTO> toDtoOrderIdSet(Set<Order> order) {
        return order.stream().map(this::toDtoOrderId).collect(Collectors.toSet());
    }
}
