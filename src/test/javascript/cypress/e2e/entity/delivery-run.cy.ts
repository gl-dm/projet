import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('DeliveryRun e2e test', () => {
  const deliveryRunPageUrl = '/delivery-run';
  const deliveryRunPageUrlPattern = new RegExp('/delivery-run(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'user';
  const password = Cypress.env('E2E_PASSWORD') ?? 'user';
  const deliveryRunSample = {};

  let deliveryRun;

  beforeEach(() => {
    cy.login(username, password);
  });

  beforeEach(() => {
    cy.intercept('GET', '/api/delivery-runs+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/api/delivery-runs').as('postEntityRequest');
    cy.intercept('DELETE', '/api/delivery-runs/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (deliveryRun) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/api/delivery-runs/${deliveryRun.id}`,
      }).then(() => {
        deliveryRun = undefined;
      });
    }
  });

  it('DeliveryRuns menu should load DeliveryRuns page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('delivery-run');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('DeliveryRun').should('exist');
    cy.url().should('match', deliveryRunPageUrlPattern);
  });

  describe('DeliveryRun page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(deliveryRunPageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create DeliveryRun page', () => {
        cy.get(entityCreateButtonSelector).click();
        cy.url().should('match', new RegExp('/delivery-run/new$'));
        cy.getEntityCreateUpdateHeading('DeliveryRun');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', deliveryRunPageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/api/delivery-runs',
          body: deliveryRunSample,
        }).then(({ body }) => {
          deliveryRun = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/api/delivery-runs+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              body: [deliveryRun],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(deliveryRunPageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details DeliveryRun page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('deliveryRun');
        cy.get(entityDetailsBackButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', deliveryRunPageUrlPattern);
      });

      it('edit button click should load edit DeliveryRun page and go back', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('DeliveryRun');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', deliveryRunPageUrlPattern);
      });

      it('edit button click should load edit DeliveryRun page and save', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('DeliveryRun');
        cy.get(entityCreateSaveButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', deliveryRunPageUrlPattern);
      });

      it('last delete button click should delete instance of DeliveryRun', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('deliveryRun').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click();
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', deliveryRunPageUrlPattern);

        deliveryRun = undefined;
      });
    });
  });

  describe('new DeliveryRun page', () => {
    beforeEach(() => {
      cy.visit(`${deliveryRunPageUrl}`);
      cy.get(entityCreateButtonSelector).click();
      cy.getEntityCreateUpdateHeading('DeliveryRun');
    });

    it('should create an instance of DeliveryRun', () => {
      cy.get(`[data-cy="idDelivery"]`).type('45789').should('have.value', '45789');

      cy.get(`[data-cy="address"]`).type('JBOD').should('have.value', 'JBOD');

      cy.get(`[data-cy="startTime"]`).type('2023-04-09T06:42').blur().should('have.value', '2023-04-09T06:42');

      cy.get(`[data-cy="endTime"]`).type('2023-04-09T12:06').blur().should('have.value', '2023-04-09T12:06');

      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response.statusCode).to.equal(201);
        deliveryRun = response.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response.statusCode).to.equal(200);
      });
      cy.url().should('match', deliveryRunPageUrlPattern);
    });
  });
});
