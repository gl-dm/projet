/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import VueRouter from 'vue-router';

import * as config from '@/shared/config/config';
import DeliveryRunDetailComponent from '@/entities/delivery-run/delivery-run-details.vue';
import DeliveryRunClass from '@/entities/delivery-run/delivery-run-details.component';
import DeliveryRunService from '@/entities/delivery-run/delivery-run.service';
import router from '@/router';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();
localVue.use(VueRouter);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('DeliveryRun Management Detail Component', () => {
    let wrapper: Wrapper<DeliveryRunClass>;
    let comp: DeliveryRunClass;
    let deliveryRunServiceStub: SinonStubbedInstance<DeliveryRunService>;

    beforeEach(() => {
      deliveryRunServiceStub = sinon.createStubInstance<DeliveryRunService>(DeliveryRunService);

      wrapper = shallowMount<DeliveryRunClass>(DeliveryRunDetailComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: { deliveryRunService: () => deliveryRunServiceStub, alertService: () => new AlertService() },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundDeliveryRun = { id: 123 };
        deliveryRunServiceStub.find.resolves(foundDeliveryRun);

        // WHEN
        comp.retrieveDeliveryRun(123);
        await comp.$nextTick();

        // THEN
        expect(comp.deliveryRun).toBe(foundDeliveryRun);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundDeliveryRun = { id: 123 };
        deliveryRunServiceStub.find.resolves(foundDeliveryRun);

        // WHEN
        comp.beforeRouteEnter({ params: { deliveryRunId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.deliveryRun).toBe(foundDeliveryRun);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
