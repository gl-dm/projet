package polytech.info.gl.repository;

import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import polytech.info.gl.domain.Restaurant;

public interface RestaurantRepositoryWithBagRelationships {
    Optional<Restaurant> fetchBagRelationships(Optional<Restaurant> restaurant);

    List<Restaurant> fetchBagRelationships(List<Restaurant> restaurants);

    Page<Restaurant> fetchBagRelationships(Page<Restaurant> restaurants);
}
