import { Component, Vue, Inject } from 'vue-property-decorator';
import Vue2Filters from 'vue2-filters';
import { IDeliveryRun } from '@/shared/model/delivery-run.model';

import DeliveryRunService from './delivery-run.service';
import AlertService from '@/shared/alert/alert.service';

@Component({
  mixins: [Vue2Filters.mixin],
})
export default class DeliveryRun extends Vue {
  @Inject('deliveryRunService') private deliveryRunService: () => DeliveryRunService;
  @Inject('alertService') private alertService: () => AlertService;

  private removeId: number = null;

  public deliveryRuns: IDeliveryRun[] = [];

  public isFetching = false;

  public mounted(): void {
    this.retrieveAllDeliveryRuns();
  }

  public clear(): void {
    this.retrieveAllDeliveryRuns();
  }

  public retrieveAllDeliveryRuns(): void {
    this.isFetching = true;
    this.deliveryRunService()
      .retrieve()
      .then(
        res => {
          this.deliveryRuns = res.data;
          this.isFetching = false;
        },
        err => {
          this.isFetching = false;
          this.alertService().showHttpError(this, err.response);
        }
      );
  }

  public handleSyncList(): void {
    this.clear();
  }

  public prepareRemove(instance: IDeliveryRun): void {
    this.removeId = instance.id;
    if (<any>this.$refs.removeEntity) {
      (<any>this.$refs.removeEntity).show();
    }
  }

  public removeDeliveryRun(): void {
    this.deliveryRunService()
      .delete(this.removeId)
      .then(() => {
        const message = this.$t('blogApp.deliveryRun.deleted', { param: this.removeId });
        this.$bvToast.toast(message.toString(), {
          toaster: 'b-toaster-top-center',
          title: 'Info',
          variant: 'danger',
          solid: true,
          autoHideDelay: 5000,
        });
        this.removeId = null;
        this.retrieveAllDeliveryRuns();
        this.closeDialog();
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public closeDialog(): void {
    (<any>this.$refs.removeEntity).hide();
  }
}
