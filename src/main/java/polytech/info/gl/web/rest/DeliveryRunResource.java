package polytech.info.gl.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.StreamSupport;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import polytech.info.gl.repository.DeliveryRunRepository;
import polytech.info.gl.service.DeliveryRunService;
import polytech.info.gl.service.dto.DeliveryRunDTO;
import polytech.info.gl.web.rest.errors.BadRequestAlertException;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link polytech.info.gl.domain.DeliveryRun}.
 */
@RestController
@RequestMapping("/api")
public class DeliveryRunResource {

    private final Logger log = LoggerFactory.getLogger(DeliveryRunResource.class);

    private static final String ENTITY_NAME = "deliveryRun";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DeliveryRunService deliveryRunService;

    private final DeliveryRunRepository deliveryRunRepository;

    public DeliveryRunResource(DeliveryRunService deliveryRunService, DeliveryRunRepository deliveryRunRepository) {
        this.deliveryRunService = deliveryRunService;
        this.deliveryRunRepository = deliveryRunRepository;
    }

    /**
     * {@code POST  /delivery-runs} : Create a new deliveryRun.
     *
     * @param deliveryRunDTO the deliveryRunDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new deliveryRunDTO, or with status {@code 400 (Bad Request)} if the deliveryRun has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/delivery-runs")
    public ResponseEntity<DeliveryRunDTO> createDeliveryRun(@Valid @RequestBody DeliveryRunDTO deliveryRunDTO) throws URISyntaxException {
        log.debug("REST request to save DeliveryRun : {}", deliveryRunDTO);
        if (deliveryRunDTO.getId() != null) {
            throw new BadRequestAlertException("A new deliveryRun cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DeliveryRunDTO result = deliveryRunService.save(deliveryRunDTO);
        return ResponseEntity
            .created(new URI("/api/delivery-runs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /delivery-runs/:id} : Updates an existing deliveryRun.
     *
     * @param id the id of the deliveryRunDTO to save.
     * @param deliveryRunDTO the deliveryRunDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated deliveryRunDTO,
     * or with status {@code 400 (Bad Request)} if the deliveryRunDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the deliveryRunDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/delivery-runs/{id}")
    public ResponseEntity<DeliveryRunDTO> updateDeliveryRun(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody DeliveryRunDTO deliveryRunDTO
    ) throws URISyntaxException {
        log.debug("REST request to update DeliveryRun : {}, {}", id, deliveryRunDTO);
        if (deliveryRunDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, deliveryRunDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!deliveryRunRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        DeliveryRunDTO result = deliveryRunService.update(deliveryRunDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, deliveryRunDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /delivery-runs/:id} : Partial updates given fields of an existing deliveryRun, field will ignore if it is null
     *
     * @param id the id of the deliveryRunDTO to save.
     * @param deliveryRunDTO the deliveryRunDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated deliveryRunDTO,
     * or with status {@code 400 (Bad Request)} if the deliveryRunDTO is not valid,
     * or with status {@code 404 (Not Found)} if the deliveryRunDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the deliveryRunDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/delivery-runs/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<DeliveryRunDTO> partialUpdateDeliveryRun(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody DeliveryRunDTO deliveryRunDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update DeliveryRun partially : {}, {}", id, deliveryRunDTO);
        if (deliveryRunDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, deliveryRunDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!deliveryRunRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<DeliveryRunDTO> result = deliveryRunService.partialUpdate(deliveryRunDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, deliveryRunDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /delivery-runs} : get all the deliveryRuns.
     *
     * @param filter the filter of the request.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of deliveryRuns in body.
     */
    @GetMapping("/delivery-runs")
    public List<DeliveryRunDTO> getAllDeliveryRuns(@RequestParam(required = false) String filter) {
        if ("order-is-null".equals(filter)) {
            log.debug("REST request to get all DeliveryRuns where order is null");
            return deliveryRunService.findAllWhereOrderIsNull();
        }
        log.debug("REST request to get all DeliveryRuns");
        return deliveryRunService.findAll();
    }

    /**
     * {@code GET  /delivery-runs/:id} : get the "id" deliveryRun.
     *
     * @param id the id of the deliveryRunDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the deliveryRunDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/delivery-runs/{id}")
    public ResponseEntity<DeliveryRunDTO> getDeliveryRun(@PathVariable Long id) {
        log.debug("REST request to get DeliveryRun : {}", id);
        Optional<DeliveryRunDTO> deliveryRunDTO = deliveryRunService.findOne(id);
        return ResponseUtil.wrapOrNotFound(deliveryRunDTO);
    }

    /**
     * {@code DELETE  /delivery-runs/:id} : delete the "id" deliveryRun.
     *
     * @param id the id of the deliveryRunDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/delivery-runs/{id}")
    public ResponseEntity<Void> deleteDeliveryRun(@PathVariable Long id) {
        log.debug("REST request to delete DeliveryRun : {}", id);
        deliveryRunService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
