import { Authority } from '@/shared/security/authority';
/* tslint:disable */
// prettier-ignore
const Entities = () => import('@/entities/entities.vue');

// prettier-ignore
const Restaurant = () => import('@/entities/restaurant/restaurant.vue');
// prettier-ignore
const RestaurantUpdate = () => import('@/entities/restaurant/restaurant-update.vue');
// prettier-ignore
const RestaurantDetails = () => import('@/entities/restaurant/restaurant-details.vue');
// prettier-ignore
const Courier = () => import('@/entities/courier/courier.vue');
// prettier-ignore
const CourierUpdate = () => import('@/entities/courier/courier-update.vue');
// prettier-ignore
const CourierDetails = () => import('@/entities/courier/courier-details.vue');
// prettier-ignore
const Customer = () => import('@/entities/customer/customer.vue');
// prettier-ignore
const CustomerUpdate = () => import('@/entities/customer/customer-update.vue');
// prettier-ignore
const CustomerDetails = () => import('@/entities/customer/customer-details.vue');
// prettier-ignore
const Product = () => import('@/entities/product/product.vue');
// prettier-ignore
const ProductUpdate = () => import('@/entities/product/product-update.vue');
// prettier-ignore
const ProductDetails = () => import('@/entities/product/product-details.vue');
// prettier-ignore
const Order = () => import('@/entities/order/order.vue');
// prettier-ignore
const OrderUpdate = () => import('@/entities/order/order-update.vue');
// prettier-ignore
const OrderDetails = () => import('@/entities/order/order-details.vue');
// prettier-ignore
const DeliveryRun = () => import('@/entities/delivery-run/delivery-run.vue');
// prettier-ignore
const DeliveryRunUpdate = () => import('@/entities/delivery-run/delivery-run-update.vue');
// prettier-ignore
const DeliveryRunDetails = () => import('@/entities/delivery-run/delivery-run-details.vue');
// prettier-ignore
const Coop = () => import('@/entities/coop/coop.vue');
// prettier-ignore
const CoopUpdate = () => import('@/entities/coop/coop-update.vue');
// prettier-ignore
const CoopDetails = () => import('@/entities/coop/coop-details.vue');
// jhipster-needle-add-entity-to-router-import - JHipster will import entities to the router here

export default {
  path: '/',
  component: Entities,
  children: [
    {
      path: 'restaurant',
      name: 'Restaurant',
      component: Restaurant,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'restaurant/new',
      name: 'RestaurantCreate',
      component: RestaurantUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'restaurant/:restaurantId/edit',
      name: 'RestaurantEdit',
      component: RestaurantUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'restaurant/:restaurantId/view',
      name: 'RestaurantView',
      component: RestaurantDetails,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'courier',
      name: 'Courier',
      component: Courier,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'courier/new',
      name: 'CourierCreate',
      component: CourierUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'courier/:courierId/edit',
      name: 'CourierEdit',
      component: CourierUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'courier/:courierId/view',
      name: 'CourierView',
      component: CourierDetails,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'customer',
      name: 'Customer',
      component: Customer,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'customer/new',
      name: 'CustomerCreate',
      component: CustomerUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'customer/:customerId/edit',
      name: 'CustomerEdit',
      component: CustomerUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'customer/:customerId/view',
      name: 'CustomerView',
      component: CustomerDetails,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'product',
      name: 'Product',
      component: Product,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'product/new',
      name: 'ProductCreate',
      component: ProductUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'product/:productId/edit',
      name: 'ProductEdit',
      component: ProductUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'product/:productId/view',
      name: 'ProductView',
      component: ProductDetails,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'order',
      name: 'Order',
      component: Order,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'order/new',
      name: 'OrderCreate',
      component: OrderUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'order/:orderId/edit',
      name: 'OrderEdit',
      component: OrderUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'order/:orderId/view',
      name: 'OrderView',
      component: OrderDetails,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'delivery-run',
      name: 'DeliveryRun',
      component: DeliveryRun,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'delivery-run/new',
      name: 'DeliveryRunCreate',
      component: DeliveryRunUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'delivery-run/:deliveryRunId/edit',
      name: 'DeliveryRunEdit',
      component: DeliveryRunUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'delivery-run/:deliveryRunId/view',
      name: 'DeliveryRunView',
      component: DeliveryRunDetails,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'coop',
      name: 'Coop',
      component: Coop,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'coop/new',
      name: 'CoopCreate',
      component: CoopUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'coop/:coopId/edit',
      name: 'CoopEdit',
      component: CoopUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'coop/:coopId/view',
      name: 'CoopView',
      component: CoopDetails,
      meta: { authorities: [Authority.USER] },
    },
    // jhipster-needle-add-entity-to-router - JHipster will add entities to the router here
  ],
};
