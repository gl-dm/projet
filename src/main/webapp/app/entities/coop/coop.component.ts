import { Component, Vue, Inject } from 'vue-property-decorator';
import Vue2Filters from 'vue2-filters';
import { ICoop } from '@/shared/model/coop.model';

import CoopService from './coop.service';
import AlertService from '@/shared/alert/alert.service';

@Component({
  mixins: [Vue2Filters.mixin],
})
export default class Coop extends Vue {
  @Inject('coopService') private coopService: () => CoopService;
  @Inject('alertService') private alertService: () => AlertService;

  private removeId: number = null;

  public coops: ICoop[] = [];

  public isFetching = false;

  public mounted(): void {
    this.retrieveAllCoops();
  }

  public clear(): void {
    this.retrieveAllCoops();
  }

  public retrieveAllCoops(): void {
    this.isFetching = true;
    this.coopService()
      .retrieve()
      .then(
        res => {
          this.coops = res.data;
          this.isFetching = false;
        },
        err => {
          this.isFetching = false;
          this.alertService().showHttpError(this, err.response);
        }
      );
  }

  public handleSyncList(): void {
    this.clear();
  }

  public prepareRemove(instance: ICoop): void {
    this.removeId = instance.id;
    if (<any>this.$refs.removeEntity) {
      (<any>this.$refs.removeEntity).show();
    }
  }

  public removeCoop(): void {
    this.coopService()
      .delete(this.removeId)
      .then(() => {
        const message = this.$t('blogApp.coop.deleted', { param: this.removeId });
        this.$bvToast.toast(message.toString(), {
          toaster: 'b-toaster-top-center',
          title: 'Info',
          variant: 'danger',
          solid: true,
          autoHideDelay: 5000,
        });
        this.removeId = null;
        this.retrieveAllCoops();
        this.closeDialog();
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public closeDialog(): void {
    (<any>this.$refs.removeEntity).hide();
  }
}
