package polytech.info.gl.repository;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.stream.IntStream;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hibernate.annotations.QueryHints;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import polytech.info.gl.domain.Restaurant;

/**
 * Utility repository to load bag relationships based on https://vladmihalcea.com/hibernate-multiplebagfetchexception/
 */
public class RestaurantRepositoryWithBagRelationshipsImpl implements RestaurantRepositoryWithBagRelationships {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Optional<Restaurant> fetchBagRelationships(Optional<Restaurant> restaurant) {
        return restaurant.map(this::fetchProducts);
    }

    @Override
    public Page<Restaurant> fetchBagRelationships(Page<Restaurant> restaurants) {
        return new PageImpl<>(fetchBagRelationships(restaurants.getContent()), restaurants.getPageable(), restaurants.getTotalElements());
    }

    @Override
    public List<Restaurant> fetchBagRelationships(List<Restaurant> restaurants) {
        return Optional.of(restaurants).map(this::fetchProducts).orElse(Collections.emptyList());
    }

    Restaurant fetchProducts(Restaurant result) {
        return entityManager
            .createQuery(
                "select restaurant from Restaurant restaurant left join fetch restaurant.products where restaurant is :restaurant",
                Restaurant.class
            )
            .setParameter("restaurant", result)
            .setHint(QueryHints.PASS_DISTINCT_THROUGH, false)
            .getSingleResult();
    }

    List<Restaurant> fetchProducts(List<Restaurant> restaurants) {
        HashMap<Object, Integer> order = new HashMap<>();
        IntStream.range(0, restaurants.size()).forEach(index -> order.put(restaurants.get(index).getId(), index));
        List<Restaurant> result = entityManager
            .createQuery(
                "select distinct restaurant from Restaurant restaurant left join fetch restaurant.products where restaurant in :restaurants",
                Restaurant.class
            )
            .setParameter("restaurants", restaurants)
            .setHint(QueryHints.PASS_DISTINCT_THROUGH, false)
            .getResultList();
        Collections.sort(result, (o1, o2) -> Integer.compare(order.get(o1.getId()), order.get(o2.getId())));
        return result;
    }
}
