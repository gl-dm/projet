import { ICoop } from '@/shared/model/coop.model';

export interface ICourier {
  id?: number;
  nameC?: string;
  idC?: number | null;
  phone?: string;
  email?: string;
  maxCapacity?: number;
  deadline?: string | null;
  coop?: ICoop | null;
}

export class Courier implements ICourier {
  constructor(
    public id?: number,
    public nameC?: string,
    public idC?: number | null,
    public phone?: string,
    public email?: string,
    public maxCapacity?: number,
    public deadline?: string | null,
    public coop?: ICoop | null
  ) {}
}
