package polytech.info.gl.service.mapper;

import org.mapstruct.*;
import polytech.info.gl.domain.Customer;
import polytech.info.gl.service.dto.CustomerDTO;

/**
 * Mapper for the entity {@link Customer} and its DTO {@link CustomerDTO}.
 */
@Mapper(componentModel = "spring")
public interface CustomerMapper extends EntityMapper<CustomerDTO, Customer> {}
