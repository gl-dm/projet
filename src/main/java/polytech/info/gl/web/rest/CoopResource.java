package polytech.info.gl.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import polytech.info.gl.repository.CoopRepository;
import polytech.info.gl.service.CoopService;
import polytech.info.gl.service.dto.CoopDTO;
import polytech.info.gl.web.rest.errors.BadRequestAlertException;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link polytech.info.gl.domain.Coop}.
 */
@RestController
@RequestMapping("/api")
public class CoopResource {

    private final Logger log = LoggerFactory.getLogger(CoopResource.class);

    private static final String ENTITY_NAME = "coop";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CoopService coopService;

    private final CoopRepository coopRepository;

    public CoopResource(CoopService coopService, CoopRepository coopRepository) {
        this.coopService = coopService;
        this.coopRepository = coopRepository;
    }

    /**
     * {@code POST  /coops} : Create a new coop.
     *
     * @param coopDTO the coopDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new coopDTO, or with status {@code 400 (Bad Request)} if the coop has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/coops")
    public ResponseEntity<CoopDTO> createCoop(@Valid @RequestBody CoopDTO coopDTO) throws URISyntaxException {
        log.debug("REST request to save Coop : {}", coopDTO);
        if (coopDTO.getId() != null) {
            throw new BadRequestAlertException("A new coop cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CoopDTO result = coopService.save(coopDTO);
        return ResponseEntity
            .created(new URI("/api/coops/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /coops/:id} : Updates an existing coop.
     *
     * @param id the id of the coopDTO to save.
     * @param coopDTO the coopDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated coopDTO,
     * or with status {@code 400 (Bad Request)} if the coopDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the coopDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/coops/{id}")
    public ResponseEntity<CoopDTO> updateCoop(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody CoopDTO coopDTO
    ) throws URISyntaxException {
        log.debug("REST request to update Coop : {}, {}", id, coopDTO);
        if (coopDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, coopDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!coopRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        CoopDTO result = coopService.update(coopDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, coopDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /coops/:id} : Partial updates given fields of an existing coop, field will ignore if it is null
     *
     * @param id the id of the coopDTO to save.
     * @param coopDTO the coopDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated coopDTO,
     * or with status {@code 400 (Bad Request)} if the coopDTO is not valid,
     * or with status {@code 404 (Not Found)} if the coopDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the coopDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/coops/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<CoopDTO> partialUpdateCoop(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody CoopDTO coopDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update Coop partially : {}, {}", id, coopDTO);
        if (coopDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, coopDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!coopRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<CoopDTO> result = coopService.partialUpdate(coopDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, coopDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /coops} : get all the coops.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of coops in body.
     */
    @GetMapping("/coops")
    public List<CoopDTO> getAllCoops() {
        log.debug("REST request to get all Coops");
        return coopService.findAll();
    }

    /**
     * {@code GET  /coops/:id} : get the "id" coop.
     *
     * @param id the id of the coopDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the coopDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/coops/{id}")
    public ResponseEntity<CoopDTO> getCoop(@PathVariable Long id) {
        log.debug("REST request to get Coop : {}", id);
        Optional<CoopDTO> coopDTO = coopService.findOne(id);
        return ResponseUtil.wrapOrNotFound(coopDTO);
    }

    /**
     * {@code DELETE  /coops/:id} : delete the "id" coop.
     *
     * @param id the id of the coopDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/coops/{id}")
    public ResponseEntity<Void> deleteCoop(@PathVariable Long id) {
        log.debug("REST request to delete Coop : {}", id);
        coopService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
