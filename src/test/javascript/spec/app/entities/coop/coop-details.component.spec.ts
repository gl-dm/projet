/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import VueRouter from 'vue-router';

import * as config from '@/shared/config/config';
import CoopDetailComponent from '@/entities/coop/coop-details.vue';
import CoopClass from '@/entities/coop/coop-details.component';
import CoopService from '@/entities/coop/coop.service';
import router from '@/router';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();
localVue.use(VueRouter);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('Coop Management Detail Component', () => {
    let wrapper: Wrapper<CoopClass>;
    let comp: CoopClass;
    let coopServiceStub: SinonStubbedInstance<CoopService>;

    beforeEach(() => {
      coopServiceStub = sinon.createStubInstance<CoopService>(CoopService);

      wrapper = shallowMount<CoopClass>(CoopDetailComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: { coopService: () => coopServiceStub, alertService: () => new AlertService() },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundCoop = { id: 123 };
        coopServiceStub.find.resolves(foundCoop);

        // WHEN
        comp.retrieveCoop(123);
        await comp.$nextTick();

        // THEN
        expect(comp.coop).toBe(foundCoop);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundCoop = { id: 123 };
        coopServiceStub.find.resolves(foundCoop);

        // WHEN
        comp.beforeRouteEnter({ params: { coopId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.coop).toBe(foundCoop);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
