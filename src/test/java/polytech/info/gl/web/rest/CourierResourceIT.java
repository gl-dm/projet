package polytech.info.gl.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import polytech.info.gl.IntegrationTest;
import polytech.info.gl.domain.Courier;
import polytech.info.gl.repository.CourierRepository;
import polytech.info.gl.service.dto.CourierDTO;
import polytech.info.gl.service.mapper.CourierMapper;

/**
 * Integration tests for the {@link CourierResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class CourierResourceIT {

    private static final String DEFAULT_NAME_C = "AAAAAAAAAA";
    private static final String UPDATED_NAME_C = "BBBBBBBBBB";

    private static final Long DEFAULT_ID_C = 1000L;
    private static final Long UPDATED_ID_C = 1001L;

    private static final String DEFAULT_PHONE = "+(()2))(   )((";
    private static final String UPDATED_PHONE = "+) -5300() ((( ";

    private static final String DEFAULT_EMAIL = "d@6|jqF\\.}dvwIv";
    private static final String UPDATED_EMAIL = ".l\"@uKtV^.uamWd";

    private static final Integer DEFAULT_MAX_CAPACITY = 1;
    private static final Integer UPDATED_MAX_CAPACITY = 2;

    private static final String DEFAULT_DEADLINE = "AAAAAAAAAA";
    private static final String UPDATED_DEADLINE = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/couriers";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private CourierRepository courierRepository;

    @Autowired
    private CourierMapper courierMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCourierMockMvc;

    private Courier courier;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Courier createEntity(EntityManager em) {
        Courier courier = new Courier()
            .nameC(DEFAULT_NAME_C)
            .idC(DEFAULT_ID_C)
            .phone(DEFAULT_PHONE)
            .email(DEFAULT_EMAIL)
            .maxCapacity(DEFAULT_MAX_CAPACITY)
            .deadline(DEFAULT_DEADLINE);
        return courier;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Courier createUpdatedEntity(EntityManager em) {
        Courier courier = new Courier()
            .nameC(UPDATED_NAME_C)
            .idC(UPDATED_ID_C)
            .phone(UPDATED_PHONE)
            .email(UPDATED_EMAIL)
            .maxCapacity(UPDATED_MAX_CAPACITY)
            .deadline(UPDATED_DEADLINE);
        return courier;
    }

    @BeforeEach
    public void initTest() {
        courier = createEntity(em);
    }

    @Test
    @Transactional
    void createCourier() throws Exception {
        int databaseSizeBeforeCreate = courierRepository.findAll().size();
        // Create the Courier
        CourierDTO courierDTO = courierMapper.toDto(courier);
        restCourierMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(courierDTO)))
            .andExpect(status().isCreated());

        // Validate the Courier in the database
        List<Courier> courierList = courierRepository.findAll();
        assertThat(courierList).hasSize(databaseSizeBeforeCreate + 1);
        Courier testCourier = courierList.get(courierList.size() - 1);
        assertThat(testCourier.getNameC()).isEqualTo(DEFAULT_NAME_C);
        assertThat(testCourier.getIdC()).isEqualTo(DEFAULT_ID_C);
        assertThat(testCourier.getPhone()).isEqualTo(DEFAULT_PHONE);
        assertThat(testCourier.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testCourier.getMaxCapacity()).isEqualTo(DEFAULT_MAX_CAPACITY);
        assertThat(testCourier.getDeadline()).isEqualTo(DEFAULT_DEADLINE);
    }

    @Test
    @Transactional
    void createCourierWithExistingId() throws Exception {
        // Create the Courier with an existing ID
        courier.setId(1L);
        CourierDTO courierDTO = courierMapper.toDto(courier);

        int databaseSizeBeforeCreate = courierRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restCourierMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(courierDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Courier in the database
        List<Courier> courierList = courierRepository.findAll();
        assertThat(courierList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkNameCIsRequired() throws Exception {
        int databaseSizeBeforeTest = courierRepository.findAll().size();
        // set the field null
        courier.setNameC(null);

        // Create the Courier, which fails.
        CourierDTO courierDTO = courierMapper.toDto(courier);

        restCourierMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(courierDTO)))
            .andExpect(status().isBadRequest());

        List<Courier> courierList = courierRepository.findAll();
        assertThat(courierList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkPhoneIsRequired() throws Exception {
        int databaseSizeBeforeTest = courierRepository.findAll().size();
        // set the field null
        courier.setPhone(null);

        // Create the Courier, which fails.
        CourierDTO courierDTO = courierMapper.toDto(courier);

        restCourierMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(courierDTO)))
            .andExpect(status().isBadRequest());

        List<Courier> courierList = courierRepository.findAll();
        assertThat(courierList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkEmailIsRequired() throws Exception {
        int databaseSizeBeforeTest = courierRepository.findAll().size();
        // set the field null
        courier.setEmail(null);

        // Create the Courier, which fails.
        CourierDTO courierDTO = courierMapper.toDto(courier);

        restCourierMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(courierDTO)))
            .andExpect(status().isBadRequest());

        List<Courier> courierList = courierRepository.findAll();
        assertThat(courierList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkMaxCapacityIsRequired() throws Exception {
        int databaseSizeBeforeTest = courierRepository.findAll().size();
        // set the field null
        courier.setMaxCapacity(null);

        // Create the Courier, which fails.
        CourierDTO courierDTO = courierMapper.toDto(courier);

        restCourierMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(courierDTO)))
            .andExpect(status().isBadRequest());

        List<Courier> courierList = courierRepository.findAll();
        assertThat(courierList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllCouriers() throws Exception {
        // Initialize the database
        courierRepository.saveAndFlush(courier);

        // Get all the courierList
        restCourierMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(courier.getId().intValue())))
            .andExpect(jsonPath("$.[*].nameC").value(hasItem(DEFAULT_NAME_C)))
            .andExpect(jsonPath("$.[*].idC").value(hasItem(DEFAULT_ID_C.intValue())))
            .andExpect(jsonPath("$.[*].phone").value(hasItem(DEFAULT_PHONE)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].maxCapacity").value(hasItem(DEFAULT_MAX_CAPACITY)))
            .andExpect(jsonPath("$.[*].deadline").value(hasItem(DEFAULT_DEADLINE)));
    }

    @Test
    @Transactional
    void getCourier() throws Exception {
        // Initialize the database
        courierRepository.saveAndFlush(courier);

        // Get the courier
        restCourierMockMvc
            .perform(get(ENTITY_API_URL_ID, courier.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(courier.getId().intValue()))
            .andExpect(jsonPath("$.nameC").value(DEFAULT_NAME_C))
            .andExpect(jsonPath("$.idC").value(DEFAULT_ID_C.intValue()))
            .andExpect(jsonPath("$.phone").value(DEFAULT_PHONE))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL))
            .andExpect(jsonPath("$.maxCapacity").value(DEFAULT_MAX_CAPACITY))
            .andExpect(jsonPath("$.deadline").value(DEFAULT_DEADLINE));
    }

    @Test
    @Transactional
    void getNonExistingCourier() throws Exception {
        // Get the courier
        restCourierMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingCourier() throws Exception {
        // Initialize the database
        courierRepository.saveAndFlush(courier);

        int databaseSizeBeforeUpdate = courierRepository.findAll().size();

        // Update the courier
        Courier updatedCourier = courierRepository.findById(courier.getId()).get();
        // Disconnect from session so that the updates on updatedCourier are not directly saved in db
        em.detach(updatedCourier);
        updatedCourier
            .nameC(UPDATED_NAME_C)
            .idC(UPDATED_ID_C)
            .phone(UPDATED_PHONE)
            .email(UPDATED_EMAIL)
            .maxCapacity(UPDATED_MAX_CAPACITY)
            .deadline(UPDATED_DEADLINE);
        CourierDTO courierDTO = courierMapper.toDto(updatedCourier);

        restCourierMockMvc
            .perform(
                put(ENTITY_API_URL_ID, courierDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(courierDTO))
            )
            .andExpect(status().isOk());

        // Validate the Courier in the database
        List<Courier> courierList = courierRepository.findAll();
        assertThat(courierList).hasSize(databaseSizeBeforeUpdate);
        Courier testCourier = courierList.get(courierList.size() - 1);
        assertThat(testCourier.getNameC()).isEqualTo(UPDATED_NAME_C);
        assertThat(testCourier.getIdC()).isEqualTo(UPDATED_ID_C);
        assertThat(testCourier.getPhone()).isEqualTo(UPDATED_PHONE);
        assertThat(testCourier.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testCourier.getMaxCapacity()).isEqualTo(UPDATED_MAX_CAPACITY);
        assertThat(testCourier.getDeadline()).isEqualTo(UPDATED_DEADLINE);
    }

    @Test
    @Transactional
    void putNonExistingCourier() throws Exception {
        int databaseSizeBeforeUpdate = courierRepository.findAll().size();
        courier.setId(count.incrementAndGet());

        // Create the Courier
        CourierDTO courierDTO = courierMapper.toDto(courier);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCourierMockMvc
            .perform(
                put(ENTITY_API_URL_ID, courierDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(courierDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Courier in the database
        List<Courier> courierList = courierRepository.findAll();
        assertThat(courierList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchCourier() throws Exception {
        int databaseSizeBeforeUpdate = courierRepository.findAll().size();
        courier.setId(count.incrementAndGet());

        // Create the Courier
        CourierDTO courierDTO = courierMapper.toDto(courier);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCourierMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(courierDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Courier in the database
        List<Courier> courierList = courierRepository.findAll();
        assertThat(courierList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamCourier() throws Exception {
        int databaseSizeBeforeUpdate = courierRepository.findAll().size();
        courier.setId(count.incrementAndGet());

        // Create the Courier
        CourierDTO courierDTO = courierMapper.toDto(courier);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCourierMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(courierDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Courier in the database
        List<Courier> courierList = courierRepository.findAll();
        assertThat(courierList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateCourierWithPatch() throws Exception {
        // Initialize the database
        courierRepository.saveAndFlush(courier);

        int databaseSizeBeforeUpdate = courierRepository.findAll().size();

        // Update the courier using partial update
        Courier partialUpdatedCourier = new Courier();
        partialUpdatedCourier.setId(courier.getId());

        partialUpdatedCourier.email(UPDATED_EMAIL).maxCapacity(UPDATED_MAX_CAPACITY).deadline(UPDATED_DEADLINE);

        restCourierMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCourier.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCourier))
            )
            .andExpect(status().isOk());

        // Validate the Courier in the database
        List<Courier> courierList = courierRepository.findAll();
        assertThat(courierList).hasSize(databaseSizeBeforeUpdate);
        Courier testCourier = courierList.get(courierList.size() - 1);
        assertThat(testCourier.getNameC()).isEqualTo(DEFAULT_NAME_C);
        assertThat(testCourier.getIdC()).isEqualTo(DEFAULT_ID_C);
        assertThat(testCourier.getPhone()).isEqualTo(DEFAULT_PHONE);
        assertThat(testCourier.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testCourier.getMaxCapacity()).isEqualTo(UPDATED_MAX_CAPACITY);
        assertThat(testCourier.getDeadline()).isEqualTo(UPDATED_DEADLINE);
    }

    @Test
    @Transactional
    void fullUpdateCourierWithPatch() throws Exception {
        // Initialize the database
        courierRepository.saveAndFlush(courier);

        int databaseSizeBeforeUpdate = courierRepository.findAll().size();

        // Update the courier using partial update
        Courier partialUpdatedCourier = new Courier();
        partialUpdatedCourier.setId(courier.getId());

        partialUpdatedCourier
            .nameC(UPDATED_NAME_C)
            .idC(UPDATED_ID_C)
            .phone(UPDATED_PHONE)
            .email(UPDATED_EMAIL)
            .maxCapacity(UPDATED_MAX_CAPACITY)
            .deadline(UPDATED_DEADLINE);

        restCourierMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCourier.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCourier))
            )
            .andExpect(status().isOk());

        // Validate the Courier in the database
        List<Courier> courierList = courierRepository.findAll();
        assertThat(courierList).hasSize(databaseSizeBeforeUpdate);
        Courier testCourier = courierList.get(courierList.size() - 1);
        assertThat(testCourier.getNameC()).isEqualTo(UPDATED_NAME_C);
        assertThat(testCourier.getIdC()).isEqualTo(UPDATED_ID_C);
        assertThat(testCourier.getPhone()).isEqualTo(UPDATED_PHONE);
        assertThat(testCourier.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testCourier.getMaxCapacity()).isEqualTo(UPDATED_MAX_CAPACITY);
        assertThat(testCourier.getDeadline()).isEqualTo(UPDATED_DEADLINE);
    }

    @Test
    @Transactional
    void patchNonExistingCourier() throws Exception {
        int databaseSizeBeforeUpdate = courierRepository.findAll().size();
        courier.setId(count.incrementAndGet());

        // Create the Courier
        CourierDTO courierDTO = courierMapper.toDto(courier);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCourierMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, courierDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(courierDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Courier in the database
        List<Courier> courierList = courierRepository.findAll();
        assertThat(courierList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchCourier() throws Exception {
        int databaseSizeBeforeUpdate = courierRepository.findAll().size();
        courier.setId(count.incrementAndGet());

        // Create the Courier
        CourierDTO courierDTO = courierMapper.toDto(courier);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCourierMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(courierDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Courier in the database
        List<Courier> courierList = courierRepository.findAll();
        assertThat(courierList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamCourier() throws Exception {
        int databaseSizeBeforeUpdate = courierRepository.findAll().size();
        courier.setId(count.incrementAndGet());

        // Create the Courier
        CourierDTO courierDTO = courierMapper.toDto(courier);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCourierMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(courierDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Courier in the database
        List<Courier> courierList = courierRepository.findAll();
        assertThat(courierList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteCourier() throws Exception {
        // Initialize the database
        courierRepository.saveAndFlush(courier);

        int databaseSizeBeforeDelete = courierRepository.findAll().size();

        // Delete the courier
        restCourierMockMvc
            .perform(delete(ENTITY_API_URL_ID, courier.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Courier> courierList = courierRepository.findAll();
        assertThat(courierList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
