package polytech.info.gl.domain;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import polytech.info.gl.web.rest.TestUtil;

class DeliveryRunTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DeliveryRun.class);
        DeliveryRun deliveryRun1 = new DeliveryRun();
        deliveryRun1.setId(1L);
        DeliveryRun deliveryRun2 = new DeliveryRun();
        deliveryRun2.setId(deliveryRun1.getId());
        assertThat(deliveryRun1).isEqualTo(deliveryRun2);
        deliveryRun2.setId(2L);
        assertThat(deliveryRun1).isNotEqualTo(deliveryRun2);
        deliveryRun1.setId(null);
        assertThat(deliveryRun1).isNotEqualTo(deliveryRun2);
    }
}
