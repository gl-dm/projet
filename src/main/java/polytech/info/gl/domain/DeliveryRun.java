package polytech.info.gl.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.Instant;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A DeliveryRun.
 */
@Entity
@Table(name = "delivery_run")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class DeliveryRun implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "id_delivery", unique = true)
    private Long idDelivery;

    @Size(min = 3)
    @Column(name = "address")
    private String address;

    @Column(name = "start_time")
    private Instant startTime;

    @Column(name = "end_time")
    private Instant endTime;

    @ManyToOne
    @JsonIgnoreProperties(value = { "coop" }, allowSetters = true)
    private Courier coursier;

    @JsonIgnoreProperties(value = { "deliveryRun", "customer", "courier", "products" }, allowSetters = true)
    @OneToOne(mappedBy = "deliveryRun")
    private Order order;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public DeliveryRun id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdDelivery() {
        return this.idDelivery;
    }

    public DeliveryRun idDelivery(Long idDelivery) {
        this.setIdDelivery(idDelivery);
        return this;
    }

    public void setIdDelivery(Long idDelivery) {
        this.idDelivery = idDelivery;
    }

    public String getAddress() {
        return this.address;
    }

    public DeliveryRun address(String address) {
        this.setAddress(address);
        return this;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Instant getStartTime() {
        return this.startTime;
    }

    public DeliveryRun startTime(Instant startTime) {
        this.setStartTime(startTime);
        return this;
    }

    public void setStartTime(Instant startTime) {
        this.startTime = startTime;
    }

    public Instant getEndTime() {
        return this.endTime;
    }

    public DeliveryRun endTime(Instant endTime) {
        this.setEndTime(endTime);
        return this;
    }

    public void setEndTime(Instant endTime) {
        this.endTime = endTime;
    }

    public Courier getCoursier() {
        return this.coursier;
    }

    public void setCoursier(Courier courier) {
        this.coursier = courier;
    }

    public DeliveryRun coursier(Courier courier) {
        this.setCoursier(courier);
        return this;
    }

    public Order getOrder() {
        return this.order;
    }

    public void setOrder(Order order) {
        if (this.order != null) {
            this.order.setDeliveryRun(null);
        }
        if (order != null) {
            order.setDeliveryRun(this);
        }
        this.order = order;
    }

    public DeliveryRun order(Order order) {
        this.setOrder(order);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DeliveryRun)) {
            return false;
        }
        return id != null && id.equals(((DeliveryRun) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "DeliveryRun{" +
            "id=" + getId() +
            ", idDelivery=" + getIdDelivery() +
            ", address='" + getAddress() + "'" +
            ", startTime='" + getStartTime() + "'" +
            ", endTime='" + getEndTime() + "'" +
            "}";
    }
}
