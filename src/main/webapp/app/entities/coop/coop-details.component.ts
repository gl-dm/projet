import { Component, Vue, Inject } from 'vue-property-decorator';

import { ICoop } from '@/shared/model/coop.model';
import CoopService from './coop.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class CoopDetails extends Vue {
  @Inject('coopService') private coopService: () => CoopService;
  @Inject('alertService') private alertService: () => AlertService;

  public coop: ICoop = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.coopId) {
        vm.retrieveCoop(to.params.coopId);
      }
    });
  }

  public retrieveCoop(coopId) {
    this.coopService()
      .find(coopId)
      .then(res => {
        this.coop = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
