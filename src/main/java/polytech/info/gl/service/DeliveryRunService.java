package polytech.info.gl.service;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import polytech.info.gl.domain.DeliveryRun;
import polytech.info.gl.repository.DeliveryRunRepository;
import polytech.info.gl.service.dto.DeliveryRunDTO;
import polytech.info.gl.service.mapper.DeliveryRunMapper;

/**
 * Service Implementation for managing {@link DeliveryRun}.
 */
@Service
@Transactional
public class DeliveryRunService {

    private final Logger log = LoggerFactory.getLogger(DeliveryRunService.class);

    private final DeliveryRunRepository deliveryRunRepository;

    private final DeliveryRunMapper deliveryRunMapper;

    public DeliveryRunService(DeliveryRunRepository deliveryRunRepository, DeliveryRunMapper deliveryRunMapper) {
        this.deliveryRunRepository = deliveryRunRepository;
        this.deliveryRunMapper = deliveryRunMapper;
    }

    /**
     * Save a deliveryRun.
     *
     * @param deliveryRunDTO the entity to save.
     * @return the persisted entity.
     */
    public DeliveryRunDTO save(DeliveryRunDTO deliveryRunDTO) {
        log.debug("Request to save DeliveryRun : {}", deliveryRunDTO);
        DeliveryRun deliveryRun = deliveryRunMapper.toEntity(deliveryRunDTO);
        deliveryRun = deliveryRunRepository.save(deliveryRun);
        return deliveryRunMapper.toDto(deliveryRun);
    }

    /**
     * Update a deliveryRun.
     *
     * @param deliveryRunDTO the entity to save.
     * @return the persisted entity.
     */
    public DeliveryRunDTO update(DeliveryRunDTO deliveryRunDTO) {
        log.debug("Request to update DeliveryRun : {}", deliveryRunDTO);
        DeliveryRun deliveryRun = deliveryRunMapper.toEntity(deliveryRunDTO);
        deliveryRun = deliveryRunRepository.save(deliveryRun);
        return deliveryRunMapper.toDto(deliveryRun);
    }

    /**
     * Partially update a deliveryRun.
     *
     * @param deliveryRunDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<DeliveryRunDTO> partialUpdate(DeliveryRunDTO deliveryRunDTO) {
        log.debug("Request to partially update DeliveryRun : {}", deliveryRunDTO);

        return deliveryRunRepository
            .findById(deliveryRunDTO.getId())
            .map(existingDeliveryRun -> {
                deliveryRunMapper.partialUpdate(existingDeliveryRun, deliveryRunDTO);

                return existingDeliveryRun;
            })
            .map(deliveryRunRepository::save)
            .map(deliveryRunMapper::toDto);
    }

    /**
     * Get all the deliveryRuns.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<DeliveryRunDTO> findAll() {
        log.debug("Request to get all DeliveryRuns");
        return deliveryRunRepository.findAll().stream().map(deliveryRunMapper::toDto).collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     *  Get all the deliveryRuns where Order is {@code null}.
     *  @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<DeliveryRunDTO> findAllWhereOrderIsNull() {
        log.debug("Request to get all deliveryRuns where Order is null");
        return StreamSupport
            .stream(deliveryRunRepository.findAll().spliterator(), false)
            .filter(deliveryRun -> deliveryRun.getOrder() == null)
            .map(deliveryRunMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one deliveryRun by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<DeliveryRunDTO> findOne(Long id) {
        log.debug("Request to get DeliveryRun : {}", id);
        return deliveryRunRepository.findById(id).map(deliveryRunMapper::toDto);
    }

    /**
     * Delete the deliveryRun by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete DeliveryRun : {}", id);
        deliveryRunRepository.deleteById(id);
    }
}
