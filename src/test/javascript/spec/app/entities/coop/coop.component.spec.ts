/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import { ToastPlugin } from 'bootstrap-vue';

import * as config from '@/shared/config/config';
import CoopComponent from '@/entities/coop/coop.vue';
import CoopClass from '@/entities/coop/coop.component';
import CoopService from '@/entities/coop/coop.service';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();
localVue.use(ToastPlugin);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('b-badge', {});
localVue.directive('b-modal', {});
localVue.component('b-button', {});
localVue.component('router-link', {});

const bModalStub = {
  render: () => {},
  methods: {
    hide: () => {},
    show: () => {},
  },
};

describe('Component Tests', () => {
  describe('Coop Management Component', () => {
    let wrapper: Wrapper<CoopClass>;
    let comp: CoopClass;
    let coopServiceStub: SinonStubbedInstance<CoopService>;

    beforeEach(() => {
      coopServiceStub = sinon.createStubInstance<CoopService>(CoopService);
      coopServiceStub.retrieve.resolves({ headers: {} });

      wrapper = shallowMount<CoopClass>(CoopComponent, {
        store,
        i18n,
        localVue,
        stubs: { bModal: bModalStub as any },
        provide: {
          coopService: () => coopServiceStub,
          alertService: () => new AlertService(),
        },
      });
      comp = wrapper.vm;
    });

    it('Should call load all on init', async () => {
      // GIVEN
      coopServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });

      // WHEN
      comp.retrieveAllCoops();
      await comp.$nextTick();

      // THEN
      expect(coopServiceStub.retrieve.called).toBeTruthy();
      expect(comp.coops[0]).toEqual(expect.objectContaining({ id: 123 }));
    });
    it('Should call delete service on confirmDelete', async () => {
      // GIVEN
      coopServiceStub.delete.resolves({});

      // WHEN
      comp.prepareRemove({ id: 123 });
      expect(coopServiceStub.retrieve.callCount).toEqual(1);

      comp.removeCoop();
      await comp.$nextTick();

      // THEN
      expect(coopServiceStub.delete.called).toBeTruthy();
      expect(coopServiceStub.retrieve.callCount).toEqual(2);
    });
  });
});
