package polytech.info.gl.service.mapper;

import org.mapstruct.*;
import polytech.info.gl.domain.Coop;
import polytech.info.gl.service.dto.CoopDTO;

/**
 * Mapper for the entity {@link Coop} and its DTO {@link CoopDTO}.
 */
@Mapper(componentModel = "spring")
public interface CoopMapper extends EntityMapper<CoopDTO, Coop> {}
