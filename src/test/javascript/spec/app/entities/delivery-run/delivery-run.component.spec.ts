/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import { ToastPlugin } from 'bootstrap-vue';

import * as config from '@/shared/config/config';
import DeliveryRunComponent from '@/entities/delivery-run/delivery-run.vue';
import DeliveryRunClass from '@/entities/delivery-run/delivery-run.component';
import DeliveryRunService from '@/entities/delivery-run/delivery-run.service';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();
localVue.use(ToastPlugin);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('b-badge', {});
localVue.directive('b-modal', {});
localVue.component('b-button', {});
localVue.component('router-link', {});

const bModalStub = {
  render: () => {},
  methods: {
    hide: () => {},
    show: () => {},
  },
};

describe('Component Tests', () => {
  describe('DeliveryRun Management Component', () => {
    let wrapper: Wrapper<DeliveryRunClass>;
    let comp: DeliveryRunClass;
    let deliveryRunServiceStub: SinonStubbedInstance<DeliveryRunService>;

    beforeEach(() => {
      deliveryRunServiceStub = sinon.createStubInstance<DeliveryRunService>(DeliveryRunService);
      deliveryRunServiceStub.retrieve.resolves({ headers: {} });

      wrapper = shallowMount<DeliveryRunClass>(DeliveryRunComponent, {
        store,
        i18n,
        localVue,
        stubs: { bModal: bModalStub as any },
        provide: {
          deliveryRunService: () => deliveryRunServiceStub,
          alertService: () => new AlertService(),
        },
      });
      comp = wrapper.vm;
    });

    it('Should call load all on init', async () => {
      // GIVEN
      deliveryRunServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });

      // WHEN
      comp.retrieveAllDeliveryRuns();
      await comp.$nextTick();

      // THEN
      expect(deliveryRunServiceStub.retrieve.called).toBeTruthy();
      expect(comp.deliveryRuns[0]).toEqual(expect.objectContaining({ id: 123 }));
    });
    it('Should call delete service on confirmDelete', async () => {
      // GIVEN
      deliveryRunServiceStub.delete.resolves({});

      // WHEN
      comp.prepareRemove({ id: 123 });
      expect(deliveryRunServiceStub.retrieve.callCount).toEqual(1);

      comp.removeDeliveryRun();
      await comp.$nextTick();

      // THEN
      expect(deliveryRunServiceStub.delete.called).toBeTruthy();
      expect(deliveryRunServiceStub.retrieve.callCount).toEqual(2);
    });
  });
});
