package polytech.info.gl.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import polytech.info.gl.web.rest.TestUtil;

class CoopDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(CoopDTO.class);
        CoopDTO coopDTO1 = new CoopDTO();
        coopDTO1.setId(1L);
        CoopDTO coopDTO2 = new CoopDTO();
        assertThat(coopDTO1).isNotEqualTo(coopDTO2);
        coopDTO2.setId(coopDTO1.getId());
        assertThat(coopDTO1).isEqualTo(coopDTO2);
        coopDTO2.setId(2L);
        assertThat(coopDTO1).isNotEqualTo(coopDTO2);
        coopDTO1.setId(null);
        assertThat(coopDTO1).isNotEqualTo(coopDTO2);
    }
}
