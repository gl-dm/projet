package polytech.info.gl.service.dto;

import java.io.Serializable;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link polytech.info.gl.domain.Coop} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class CoopDTO implements Serializable {

    private Long id;

    private String nameCoop;

    @NotNull
    private Long idCoop;

    private String zone;

    private String addressCoop;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNameCoop() {
        return nameCoop;
    }

    public void setNameCoop(String nameCoop) {
        this.nameCoop = nameCoop;
    }

    public Long getIdCoop() {
        return idCoop;
    }

    public void setIdCoop(Long idCoop) {
        this.idCoop = idCoop;
    }

    public String getZone() {
        return zone;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }

    public String getAddressCoop() {
        return addressCoop;
    }

    public void setAddressCoop(String addressCoop) {
        this.addressCoop = addressCoop;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CoopDTO)) {
            return false;
        }

        CoopDTO coopDTO = (CoopDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, coopDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CoopDTO{" +
            "id=" + getId() +
            ", nameCoop='" + getNameCoop() + "'" +
            ", idCoop=" + getIdCoop() +
            ", zone='" + getZone() + "'" +
            ", addressCoop='" + getAddressCoop() + "'" +
            "}";
    }
}
